import 'package:wish_app_ver_2/my_present_repository.dart';

abstract class MyPresentRepository {
  Future<void> addNewMyPresent(MyPresent myPresent, String phoneNr);

  Future<void> deleteMyPresent(MyPresent myPresent, String phoneNr);

  Stream<List<MyPresent>> myPresent(String phoneNr);

  Future<void> updateMyPresent(MyPresent myPresent, String phoneNr);
}
