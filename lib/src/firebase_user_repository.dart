import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:wish_app_ver_2/profile_repository.dart';
import 'package:wish_app_ver_2/src/entities/entities.dart';

class FirebaseProfileRepository implements ProfileRepository {
  @override
  Future<void> addNewProfile(Profile profile, String phoneNr) {
    return Firestore.instance
        .collection('profiles')
        .document('$phoneNr')
        .collection('user')
        .add(profile.toEntity().toDocument());
  }

  @override
  Future<void> deleteProfile(Profile profile) {
    return null;
  }

  @override
  Stream<List<Profile>> profile(String phoneNr) {
    return Firestore.instance
        .collection('profiles')
        .document('$phoneNr')
        .collection('user')
        .snapshots()
        .map((snapshot) {
      return snapshot.documents
          .map((doc) => Profile.fromEntity(ProfileEntity.fromSnapshot(doc)))
          .toList();
    });
  }

  @override
  Future<void> updateProfile(Profile profile,String phoneNr) {
    return Firestore.instance
        .collection('profiles')
        .document('$phoneNr')
        .collection('user')
        .document(profile.userId)
        .updateData(profile.toEntity().toDocument());
  }
}
