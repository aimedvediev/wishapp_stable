import 'package:equatable/equatable.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MemberEntity extends Equatable {
  final String id;
  final String name;
  final String phoneNr;
  final String group;
  final String photo;
  final String dateOfBD;

  const MemberEntity(
    this.id,
    this.name,
    this.phoneNr,
    this.group,
    this.photo,
    this.dateOfBD,
  );

  Map<String, Object> toJson() {
    return {
      "id": id,
      "name": name,
      "phoneNr": phoneNr,
      "group": group.toString(),
      "photo": photo,
      "dateOfBD": dateOfBD,
    };
  }

  @override
  List<Object> get props => [id, name, phoneNr, group, photo, dateOfBD];

  @override
  String toString() {
    return 'MemberEntity {id: $id, name: $name, phoneNr: $phoneNr, group: $group, photo: $photo, dateOfBD: $dateOfBD}';
  }

  static MemberEntity fromSnapshot(DocumentSnapshot snap) {
    return MemberEntity(
      snap.documentID,
      snap.data['name'],
      snap.data['phoneNr'],
      snap.data['group'].toString(),
      snap.data['photo'],
      snap.data['dateOfBD'],
    );
  }

  Map<String, Object> toDocument() {
    return {
      "name": name,
      "phoneNr": phoneNr,
      "group": group,
      "photo": photo,
      "dateOfBD": dateOfBD,
    };
  }
}
