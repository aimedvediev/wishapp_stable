import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class ProfileEntity extends Equatable {
  final String userId;
  final String name;
  final String photo;
  final String dateOfBD;

  const ProfileEntity(
    this.userId,
    this.name,
    this.photo,
    this.dateOfBD,
  );

  Map<String, Object> toJson() {
    return {
      "userId": userId,
      "name": name,
      "photo": photo,
      "dateOfBD": dateOfBD,

    };
  }

  @override
  List<Object> get props => [userId, name, photo, dateOfBD,];

  @override
  String toString() {
    return 'UserEntity {userId: $userId, name: $name, photo: $photo, dateOfBD: $dateOfBD}';
  }

  static ProfileEntity fromSnapshot(DocumentSnapshot snap) {
    return ProfileEntity(
      snap.documentID,
      snap.data['name'],
      snap.data['photo'],
      snap.data['dateOfBD'],
    );
  }

  Map<String, Object> toDocument() {
    return {
      "name": name,
      "photo": photo,
      "dateOfBD": dateOfBD,
    };
  }
}
