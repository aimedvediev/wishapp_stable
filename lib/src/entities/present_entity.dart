import 'package:equatable/equatable.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class PresentEntity extends Equatable {
  final String id;
  final String forWhomId;
  final String name;
  final String description;
  final String photo;
  final String dateOfAdd;
  final String link;
  final String groups;
  final bool received;
  final String memberPhoto;

  const PresentEntity(
    this.id,
    this.forWhomId,
    this.name,
    this.description,
    this.photo,
    this.dateOfAdd,
    this.link,
    this.groups,
    this.received,
    this.memberPhoto,
  );

  Map<String, Object> toJson() {
    return {
      "id": id,
      "forWhomId": forWhomId,
      "name": name,
      "description": description,
      "photo": photo,
      "dateOfAdd": dateOfAdd,
      "link": link,
      "groups": groups.toString(),
      "received": received,
      "memberPhoto": memberPhoto,
    };
  }

  @override
  List<Object> get props => [
        id,
        forWhomId,
        name,
        description,
        photo,
        dateOfAdd,
        link,
        groups,
        received,
        memberPhoto,
      ];

  static PresentEntity fromSnapshot(DocumentSnapshot snap) {
    return PresentEntity(
      snap.documentID,
      snap.data['forWhomId'],
      snap.data['name'],
      snap.data['description'],
      snap.data['photo'],
      snap.data['dateOfAdd'],
      snap.data['link'],
      snap.data['groups'].toString(),
      snap.data['received'],
      snap.data['memberPhoto'],
    );
  }

  Map<String, Object> toDocument() {
    return {
      "forWhomId": forWhomId,
      "name": name,
      "description": description,
      "photo": photo,
      "dateOfAdd": dateOfAdd,
      "link": link,
      "groups": groups.toString(),
      "received": received,
      "memberPhoto": memberPhoto,
    };
  }
}
