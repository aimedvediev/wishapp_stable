import 'dart:async';

import 'package:wish_app_ver_2/profile_repository.dart';

abstract class ProfileRepository {

  Future<void> addNewProfile(Profile profile, String phoneNr);

  Future<void> deleteProfile(Profile profile);

  Stream<List<Profile>> profile(String phoneNr);

  Future<void> updateProfile(Profile profile, String phoneNr);
}
