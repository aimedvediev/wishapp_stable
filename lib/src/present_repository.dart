import 'dart:async';

import 'package:wish_app_ver_2/present_repository.dart';

abstract class PresentRepository {
  Future<void> addNewPresent(Present present, String phoneNr);

  Future<void> deletePresent(Present present, String phoneNr);

  Stream<List<Present>> present(String phoneNr);

  Future<void> updatePresent(Present present, String phoneNr);
}
