import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:wish_app_ver_2/members_repository.dart';
import 'package:wish_app_ver_2/src/entities/entities.dart';

class FirebaseMemberRepository implements MembersRepository {
  @override
  Future<void> addNewMember(Member member, String phoneNr) {
    return Firestore.instance
        .collection('profiles')
        .document(phoneNr)
        .collection('members')
        .document(member.phoneNr)
        .setData(member.toEntity().toDocument());
  }

  @override
  Future<void> deleteMember(Member member, String phoneNr) async {
    FirebaseStorage.instance
        .getReferenceFromUrl(member.photo)
        .then((valued) => valued.delete());

    return Firestore.instance
        .collection('profiles')
        .document(phoneNr)
        .collection('members')
        .document(member.phoneNr)
        .delete();
  }

  @override
  Stream<List<Member>> member(String phoneNr) {
    return Firestore.instance
        .collection('profiles')
        .document(phoneNr)
        .collection('members')
        .snapshots()
        .map((snapshot) {
      return snapshot.documents
          .map((doc) => Member.fromEntity(MemberEntity.fromSnapshot(doc)))
          .toList();
    });
  }

  @override
  Future<void> updateMember(Member member, String phoneNr) {
    return Firestore.instance
        .collection('profiles')
        .document(phoneNr)
        .collection('members')
        .document(member.phoneNr)
        .updateData(member.toEntity().toDocument());
  }
}
