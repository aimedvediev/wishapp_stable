import 'package:meta/meta.dart';
import 'package:wish_app_ver_2/present_repository.dart';
import '../entities/entities.dart';

@immutable
class Present {
  String id;
  String forWhomId;
  String name;
  String description;
  String photo;
  String dateOfAdd;
  String link;
  GROUPS groups;
  bool received;
  String memberPhoto;

  Present(this.forWhomId, this.name, this.description, this.photo,
      this.dateOfAdd, this.link, this.groups, this.received, this.memberPhoto,
      {String id})
      : this.id = id;

  Present copyWith({
    String id,
    String forWhomId,
    String name,
    String description,
    String photo,
    String dateOfAdd,
    String link,
    GROUPS groups,
    bool received,
    String memberPhoto,
  }) {
    return Present(
      forWhomId ?? this.forWhomId,
      name ?? this.name,
      description ?? this.description,
      photo ?? this.photo,
      dateOfAdd ?? this.dateOfAdd,
      link ?? this.link,
      groups ?? this.groups,
      received ?? this.received,
      memberPhoto ?? this.memberPhoto,
      id: id ?? this.id,
    );
  }

  @override
  int get hashCode =>
      forWhomId.hashCode ^
      name.hashCode ^
      photo.hashCode ^
      dateOfAdd.hashCode ^
      link.hashCode ^
      groups.hashCode ^
      received.hashCode ^
      memberPhoto.hashCode ^
      id.hashCode;

  PresentEntity toEntity() {
    return PresentEntity(
      id,
      forWhomId,
      name,
      description,
      photo,
      dateOfAdd,
      link,
      groups.toString(),
      received,
      memberPhoto,
    );
  }

  static Present fromEntity(PresentEntity entity) {
    return Present(
      entity.forWhomId,
      entity.name,
      entity.description,
      entity.photo,
      entity.dateOfAdd,
      entity.link,
      entity.groups == 'GROUPS.FRIENDS'
          ? GROUPS.FRIENDS
          : entity.groups == 'GROUPS.FAMILY'
              ? GROUPS.FAMILY
              : GROUPS.COLLEAGUES,
      entity.received,
      entity.memberPhoto,
      id: entity.id,
    );
  }
}
