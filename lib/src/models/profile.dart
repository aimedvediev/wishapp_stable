import 'package:meta/meta.dart';
import 'package:wish_app_ver_2/src/entities/entities.dart';

@immutable
class Profile {
  final String userId;
  final String name;
  final String photo;
  final String dateOfBD;

  Profile(this.name, this.photo, this.dateOfBD, {String userId})
      : this.userId = userId;

  Profile copyWith({
    String userId,
    String name,
    String photo,
    String dateOfBD,
  }) {
    return Profile(
      name ?? this.name,
      photo ?? this.photo,
      dateOfBD ?? this.dateOfBD,
      userId: userId ?? this.userId,
    );
  }

  @override
  int get hashCode =>
      name.hashCode ^ photo.hashCode ^ dateOfBD.hashCode ^ userId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Profile &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          photo == other.photo &&
          dateOfBD == other.dateOfBD;

  ProfileEntity toEntity() {
    return ProfileEntity(userId, name, photo, dateOfBD);
  }

  static Profile fromEntity(ProfileEntity entity) {
    return Profile(
      entity.name,
      entity.photo,
      entity.dateOfBD,
      userId: entity.userId,
    );
  }
}
