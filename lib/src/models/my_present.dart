import 'package:meta/meta.dart';
import 'package:wish_app_ver_2/present_repository.dart';
import 'package:wish_app_ver_2/src/entities/entities.dart';

@immutable
class MyPresent {
  String id;
  String forWhomId;
  String name;
  String description;
  String photo;
  String link;
  String dateOfAdd;
  GROUPS groups;
  bool received;

  MyPresent(this.forWhomId, this.name, this.description, this.photo,
      this.dateOfAdd, this.link, this.groups, this.received,
      {String id})
      : this.id = id;

  MyPresent copyWith({
    String id,
    String forWhomId,
    String name,
    String description,
    String photo,
    String link,
    String dateOfAdd,
    GROUPS groups,
    bool received,
  }) {
    return MyPresent(
      forWhomId ?? this.forWhomId,
      name ?? this.name,
      description ?? this.description,
      photo ?? this.photo,
      dateOfAdd ?? this.dateOfAdd,
      link ?? this.link,
      groups ?? this.groups,
      received ?? this.received,
      id: id ?? this.id,
    );
  }

  @override
  int get hashCode =>
      forWhomId.hashCode ^
      name.hashCode ^
      photo.hashCode ^
      dateOfAdd.hashCode ^
      link.hashCode ^
      groups.hashCode ^
      received.hashCode ^
      id.hashCode;

  MyPresentEntity toEntity() {
    return MyPresentEntity(
      id,
      forWhomId,
      name,
      description,
      photo,
      dateOfAdd,
      link,
      groups.toString(),
      received,
    );
  }

  static MyPresent fromEntity(MyPresentEntity entity) {
    return MyPresent(
      entity.forWhomId,
      entity.name,
      entity.description,
      entity.photo,
      entity.dateOfAdd,
      entity.link,
      entity.groups == 'GROUPS.FRIENDS' ? GROUPS.FRIENDS : GROUPS.FAMILY,
      entity.received,
      id: entity.id,
    );
  }
}
