import 'package:meta/meta.dart';
import '../entities/entities.dart';

enum GROUPS { ALL, FAMILY, FRIENDS, COLLEAGUES, MYSELF }

@immutable
class Member {
  final String id;
  final String name;
  final String phoneNr;
  final GROUPS group;
  final String photo;
  final String dateOfBD;

  Member(this.name, this.phoneNr, this.photo, this.dateOfBD,
      {this.group = GROUPS.FAMILY, String id})
      : this.id = id;

  Member copyWith(
      {String id,
      String name,
      String phoneNr,
      GROUPS group,
      String photo,
      String dateOfBD}) {
    return Member(
      name ?? this.name,
      phoneNr ?? this.phoneNr,
      photo ?? this.photo,
      dateOfBD ?? this.dateOfBD,
      group: group ?? this.group,
      id: id ?? this.id,
    );
  }

  @override
  int get hashCode =>
      id.hashCode ^
      name.hashCode ^
      phoneNr.hashCode ^
      group.hashCode ^
      photo.hashCode ^
      dateOfBD.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Member &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          name == other.name &&
          phoneNr == other.phoneNr &&
          group == other.group &&
          photo == other.photo &&
          dateOfBD == other.dateOfBD;

  MemberEntity toEntity() {
    return MemberEntity(id, name, phoneNr, group.toString(), photo, dateOfBD);
  }

  static Member fromEntity(MemberEntity entity) {
    return Member(
      entity.name,
      entity.phoneNr,
      entity.photo,
      entity.dateOfBD,
      id: entity.id,
      group: entity.group == 'GROUPS.FRIENDS' ? GROUPS.FRIENDS : entity.group =='GROUPS.FAMILY'?GROUPS.FAMILY:GROUPS.COLLEAGUES,
    );
  }
}
