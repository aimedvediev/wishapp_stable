import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:wish_app_ver_2/present_repository.dart';
import 'package:wish_app_ver_2/src/entities/entities.dart';

class FirebasePresentRepository implements PresentRepository {
  @override
  Future<void> addNewPresent(Present present, String phoneNr) {
    return Firestore.instance
        .collection('profiles')
        .document(phoneNr)
        .collection('presents')
        .add(present.toEntity().toDocument());
  }

  @override
  Future<void> deletePresent(Present present, String phoneNr) {
    FirebaseStorage.instance
        .getReferenceFromUrl(present.photo)
        .then((valued) => valued.delete());
    return Firestore.instance
        .collection('profiles')
        .document(phoneNr)
        .collection('presents')
        .document(present.id)
        .delete();
  }

  @override
  Stream<List<Present>> present(String phoneNr) {
    return Firestore.instance
        .collection('profiles')
        .document(phoneNr)
        .collection('presents')
        .snapshots()
        .map((snapshot) {
      return snapshot.documents
          .map((doc) => Present.fromEntity(PresentEntity.fromSnapshot(doc)))
          .toList();
    });
  }

  @override
  Future<void> updatePresent(Present present, String phoneNr) {
    return Firestore.instance
        .collection('profiles')
        .document(phoneNr)
        .collection('presents')
        .document(present.id)
        .updateData(present.toEntity().toDocument());
    ;
  }
}
