import 'dart:async';

import 'package:wish_app_ver_2/members_repository.dart';

abstract class MembersRepository {
  Future<void> addNewMember(Member member, String phoneNr);

  Future<void> deleteMember(Member member, String phoneNr);

  Stream<List<Member>> member(String phoneNr);

  Future<void> updateMember(Member member, String phoneNr);
}
