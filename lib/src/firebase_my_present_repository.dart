import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:wish_app_ver_2/my_present_repository.dart';
import 'package:wish_app_ver_2/src/entities/entities.dart';

class FirebaseMyPresentRepository implements MyPresentRepository {
  @override
  Future<void> addNewMyPresent(MyPresent present, String phoneNr) {
    return Firestore.instance
        .collection('profiles')
        .document(phoneNr)
        .collection('myPresents')
        .add(present.toEntity().toDocument());
  }

  @override
  Future<void> deleteMyPresent(MyPresent present, String phoneNr) {
    FirebaseStorage.instance
        .getReferenceFromUrl(present.photo)
        .then((valued) => valued.delete());
    return Firestore.instance
        .collection('profiles')
        .document(phoneNr)
        .collection('myPresents')
        .document(present.id)
        .delete();
  }

  @override
  Stream<List<MyPresent>> myPresent(String memberPhoneNr) {
    return Firestore.instance
        .collection('profiles')
        .document(memberPhoneNr)
        .collection('myPresents')
        .snapshots()
        .map((snapshot) {
      return snapshot.documents
          .map((doc) => MyPresent.fromEntity(MyPresentEntity.fromSnapshot(doc)))
          .toList();
    });
  }

  @override
  Future<void> updateMyPresent(MyPresent present, String phoneNr) {
    return Firestore.instance
        .collection('profiles')
        .document(phoneNr)
        .collection('myPresents')
        .document(present.id)
        .updateData(present.toEntity().toDocument());
  }
}
