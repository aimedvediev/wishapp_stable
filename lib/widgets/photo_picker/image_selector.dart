import 'dart:io';

import 'package:flutter/material.dart';

class ImageSelector extends StatefulWidget {
  final Function() onSelection;
  File image;
  bool valid;
  String type;
  bool isEditing;
  String oldImage;

  ImageSelector(
      {this.onSelection,
      this.image,
      this.valid,
      this.type,
      this.isEditing,
      this.oldImage});

  @override
  _ImageSelectorState createState() => _ImageSelectorState();
}

class _ImageSelectorState extends State<ImageSelector> {
  @override
  Widget build(BuildContext context) {
    return widget.image == null && !widget.isEditing
        ? Container(
            height: widget.type == 'present' ? 124 : 64,
            width: widget.type == 'present' ? 96 : 64,
            decoration: BoxDecoration(
              borderRadius: widget.type == 'present'
                  ? new BorderRadius.circular(6)
                  : new BorderRadius.circular(50.0),
              border: Border.all(
                width: 1,
                color: widget.valid ? Color(0xFFF4F5F5) : Colors.red,
              ),
              color: Colors.white,
            ),
            child: ClipRRect(
              borderRadius: widget.type == 'present'
                  ? new BorderRadius.circular(6)
                  : new BorderRadius.circular(50.0),
              child: FlatButton(
                onPressed: () {
                  setState(() {
                    widget.onSelection();
                  });
                },
                child: Container(
                  child: Icon(
                    Icons.add,
                    size: 32,
                    color: widget.valid ? Colors.black : Colors.red,
                  ),
                ),
              ),
            ),
          )
        : ClipRRect(
            borderRadius: widget.type == 'present'
                ? new BorderRadius.circular(6)
                : new BorderRadius.circular(50.0),
            child: Container(
              height: widget.type == 'present' ? 124 : 64,
              width: widget.type == 'present' ? 96 : 64,
              child: FlatButton(
                padding: EdgeInsets.all(0),
                child: widget.isEditing && widget.image == null
                    ? Image.network(
                        widget.oldImage,
                        height: widget.type == 'present' ? 124 : 64,
                        width: widget.type == 'present' ? 96 : 64,
                        fit: BoxFit.cover,
                      )
                    : Image.file(
                        widget.image,
                        height: widget.type == 'present' ? 124 : 64,
                        width: widget.type == 'present' ? 96 : 64,
                        fit: BoxFit.cover,
                      ),
                onPressed: () {
                  setState(() {
                    widget.onSelection();
                  });
                },
              ),
            ),
          );
  }
}
