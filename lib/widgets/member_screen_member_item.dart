import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:wish_app_ver_2/members_repository.dart';

import 'text/name_member.dart';

class MemberItem extends StatelessWidget {
  final GestureTapCallback onTap;
  final Member member;

  MemberItem(
      {Key key,
      @required this.onTap,
      @required this.member})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 16, left: 30, right: 40, top: 0),
      child: Container(
        height: 88,
        decoration: new BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.circular(8.0),
          boxShadow: [
            new BoxShadow(
                color: Color.fromRGBO(52, 48, 45, 0.06),
                offset: new Offset(0, 8),
                blurRadius: 16)
          ],
        ),
        child: FlatButton(
          padding: EdgeInsets.all(0),
          child: ListTile(
            contentPadding: EdgeInsets.only(left: 16, top: 16, bottom: 16),
            title: NameText(member.name),
            leading: ClipRRect(
              borderRadius: BorderRadius.circular(6),
              child: Image.network(
                member.photo,
                fit: BoxFit.cover,
                height: 52,
                width: 52,
              ),
            ),
          ),
          onPressed: onTap,
        ),
      ),
    );
  }
}
