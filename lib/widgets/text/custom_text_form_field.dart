import 'package:flutter/material.dart';

class CustomTextFormField extends StatelessWidget {
  final String hint;
  final Function(String) onSave;
  final Stream<String> stream;
  final TextInputType inputType;

  CustomTextFormField({
    this.hint,
    this.onSave,
    this.stream,
    this.inputType,
  });

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: stream,
      builder: (context, snapshot) {
        return Padding(
          padding: const EdgeInsets.only(top: 12, right: 32),
          child: Container(
            height: 50,
            padding: EdgeInsets.only(left: 12),
            decoration: BoxDecoration(
              borderRadius: new BorderRadius.circular(8.0),
              border: Border.all(
                width: 1,
                color: Color(0xFFF4F5F5),
              ),
              color: Color(0xFFFFFFFF),
            ),
            child: TextFormField(
              keyboardType: inputType,
              style: TextStyle(
                color: Color(0xFF423932),
                fontSize: 16,
                letterSpacing: 0.25,
                fontFamily: 'Muli',
              ),
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: hint,
                errorText: snapshot.error,
              ),
              onChanged: onSave,
            ),
          ),
        );
      },
    );
  }
}
