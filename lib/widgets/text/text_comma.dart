import 'package:flutter/material.dart';

class TextComma extends StatelessWidget {
  final String text;

  TextComma(this.text);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontFamily: 'Muli',
        fontStyle: FontStyle.normal,
        fontWeight: FontWeight.normal,
        fontSize: 22,
        color: Color(0xFF3C332C),
      ),
    );
  }
}
