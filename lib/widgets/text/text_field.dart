import 'package:flutter/material.dart';

class TextTitleField extends StatelessWidget {
  final String text;

  TextTitleField({this.text});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Text(
        text,
        style: TextStyle(
          color: Color(0xFF655B53),
          fontSize: 14,
          fontFamily: 'OpenSans',
          letterSpacing: 0.2,
        ),
      ),
    );
  }
}
