import 'package:flutter/material.dart';

class MainText extends StatelessWidget {
  final String text;

  const MainText(this.text);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontFamily: 'Muli',
        fontWeight: FontWeight.bold,
        fontSize: 28,
        color: Color(0xFF3C332C)
      ),
    );
  }
}
