import 'package:flutter/material.dart';

class NameText extends StatelessWidget {
  final String text;

  const NameText(this.text);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
          fontFamily: 'Muli',
          fontWeight: FontWeight.bold,
          fontSize: 18,
          color: Color(0xFF3C332C)
      ),
    );
  }
}
