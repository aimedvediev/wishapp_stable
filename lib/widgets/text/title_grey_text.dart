import 'package:flutter/material.dart';

class TitleGreyText extends StatelessWidget {
  final String text;

  TitleGreyText({this.text});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: Color(0xffCACBCC),
        fontSize: 14,
        fontFamily: 'OpenSans',
      ),
    );
  }
}
