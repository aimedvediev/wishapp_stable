import 'package:flutter/material.dart';

class DateOfAddText extends StatelessWidget {
  final String text;

  DateOfAddText({this.text});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
          color: Color(0xFFCACBCC),
          fontSize: 10,
          letterSpacing: 0.2,
          fontFamily: 'OpenSans'),
    );
  }
}
