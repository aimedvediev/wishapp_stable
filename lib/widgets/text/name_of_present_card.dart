import 'package:flutter/material.dart';

class NameOfPresentCard extends StatelessWidget {
  final String text;

  NameOfPresentCard({
    this.text,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 157,
      child: Text(
        text,
        style: TextStyle(
          color: Color(0xFF3C332C),
          fontSize: 16,
          fontFamily: 'Muli',
          letterSpacing: 0.25,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }
}
