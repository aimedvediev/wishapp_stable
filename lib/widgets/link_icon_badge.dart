import 'package:flutter/material.dart';

class LinkBadge extends StatefulWidget {
  final Function() onPress;

  LinkBadge({this.onPress});

  @override
  _LinkBadgeState createState() => _LinkBadgeState();
}

class _LinkBadgeState extends State<LinkBadge> {
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: new BorderRadius.circular(7.0),
      child: Container(
        height: 32,
        width: 32,
        color: Color(0xFFFAFAF9),
        child: IconButton(
          padding: EdgeInsets.all(0),
          icon: Icon(Icons.insert_link),
          onPressed: () {
            setState(() {
              widget.onPress();
            });
          },
        ),
      ),
    );
  }
}
