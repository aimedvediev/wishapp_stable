import 'package:flutter/material.dart';

class DateCart extends StatelessWidget {
  final String date;
  final String type;

  DateCart({this.date, this.type});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 16, bottom: 13),
      child: Container(
        decoration: new BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.circular(8.0),
          boxShadow: [
            new BoxShadow(
                color: Color.fromRGBO(52, 48, 45, 0.06),
                offset: new Offset(0, 8),
                blurRadius: 24)
          ],
        ),
        padding: EdgeInsets.all(16),
        height: 76,
        width: 154,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              date,
              style: TextStyle(
                color: Color(0xffDC6C54),
                fontWeight: FontWeight.bold,
                fontFamily: 'OpenSans',
                fontSize: 16,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 6),
              child: Text(
                type,
                style: TextStyle(
                  color: Color(0xFF3C332C),
                  fontSize: 12,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'OpenSans',
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
