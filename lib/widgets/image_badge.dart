import 'package:flutter/material.dart';

class ImageBadge extends StatelessWidget {
  final String image;

  ImageBadge({
    this.image,
  });

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: new BorderRadius.circular(25.0),
      child: image == null
          ? Container(
              height: 24,
              width: 24,
            )
          : Container(
              child: Image.network(
                image,
                height: 24,
                width: 24,
                fit: BoxFit.cover,
              ),
            ),
    );
  }
}
