import 'package:flutter/material.dart';

class AvatarName extends StatelessWidget {
  final String image;
  final String name;

  AvatarName({
    this.image,
    this.name,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        ClipRRect(
          borderRadius: new BorderRadius.circular(50.0),
          child: Image.network(
            image,
            fit: BoxFit.cover,
            height: 64,
            width: 64,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 16),
          child: Text(
            name,
            style: TextStyle(
              fontSize: 22,
              fontFamily: 'Muli',
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }
}
