import 'package:flutter/material.dart';

class TextChip extends StatefulWidget {

  final String text;
  bool isActive;

  TextChip({this.text,this.isActive});

  @override
  _TextChipState createState() => _TextChipState();
}

class _TextChipState extends State<TextChip> {
  @override
  Widget build(BuildContext context) {
    return Text(
      widget.text,
      style: TextStyle(
        fontFamily: 'OpenSans',
        fontWeight: FontWeight.normal,
        color: widget.isActive ? Color(0xFF3C332C): Color(0xFFCACBCC),
        fontSize: 12,
      ),
    );
  }
}
