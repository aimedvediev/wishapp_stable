import 'package:flutter/material.dart';

import 'package:flutter_svg/flutter_svg.dart';

import 'text.dart';

class CustomChip extends StatelessWidget {
  bool isActive;
  final String chipName;

  CustomChip({this.isActive, this.chipName});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: new BoxDecoration(
        color: isActive ? Colors.white : Color(0xFFFAFAFA),
        borderRadius: new BorderRadius.circular(8.0),
        boxShadow: [
          new BoxShadow(
              color: Color.fromRGBO(52, 48, 45, 0.06),
              offset: isActive ? new Offset(0, 8) : new Offset(0, 0),
              blurRadius: isActive?16:0)
        ],
      ),
      padding: EdgeInsets.only(top: 8),
      width: 72,
      height: 88,
      child: Column(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(30),
            child: Container(
              width: 48,
              height: 48,
              child: isActive
                  ? SvgPicture.asset(
                      "lib/assets/icons/icon_$chipName" +
                          "_filter_on.svg")
                  : SvgPicture.asset(
                      "lib/assets/icons/icon_$chipName" +
                          "_filter_off.svg"),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 6),
            child: TextChip(text: chipName, isActive: isActive),
          )
        ],
      ),
    );
  }
}
