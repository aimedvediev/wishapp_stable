import 'package:flutter/material.dart';
import 'package:wish_app_ver_2/members_repository.dart';

import 'package:wish_app_ver_2/screens/my_members_screen.dart'; //todo test

import 'custom_chip.dart';

class ChipsFilter extends StatefulWidget {
  final List<GROUPS> groups;
  final Function(GROUPS) onSelectionChip;
  final String directions;
  GROUPS selectedGroup;

  ChipsFilter({
    this.groups,
    this.onSelectionChip,
    this.directions,
    this.selectedGroup,
  });

  @override
  _ChipsFilterState createState() => _ChipsFilterState();
}

class _ChipsFilterState extends State<ChipsFilter> {
  _buildChoiceList() {
    List<Widget> choices = List();

    widget.groups.forEach((item) {
      choices.add(Container(
        child: ChoiceChip(
          label: item == GROUPS.ALL
              ? CustomChip(
                  isActive: (widget.selectedGroup == item), chipName: 'All')
              : item == GROUPS.FAMILY
                  ? CustomChip(
                      isActive: (widget.selectedGroup == item),
                      chipName: 'Family')
                  : item == GROUPS.FRIENDS
                      ? CustomChip(
                          isActive: (widget.selectedGroup == item),
                          chipName: 'Friends')
                      : item == GROUPS.COLLEAGUES
                          ? CustomChip(
                              isActive: (widget.selectedGroup == item),
                              chipName: 'Work')
                          : Text('mine'),
          selected: widget.selectedGroup == item,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(8),
            ),
          ),
          pressElevation: 0,
          backgroundColor: Color(0xFFFAFAFA),
          selectedColor: Color(0xFFFAFAFA),
          onSelected: (isSelected) {
            setState(() {
              widget.selectedGroup = item;
              widget.onSelectionChip(widget.selectedGroup);
            });
          },
        ),
      ));
    });
    return choices;
  }

  @override
  Widget build(BuildContext context) {
    //todo parameter Column or Row
    return widget.directions == 'vertical'
        ? Column(
            children: _buildChoiceList(),
          )
        : SingleChildScrollView(
          child: Row(
              children: _buildChoiceList(),
            ),
        );
  }
}
