import 'package:flutter/material.dart';

class SmallButton extends StatefulWidget {
  final IconData icon;
  final Function() onPress;
  final double opacity;
  SmallButton({this.icon, this.onPress,this.opacity});

  @override
  _SmallButtonState createState() => _SmallButtonState();
}

class _SmallButtonState extends State<SmallButton> {
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(8),
      child: Container(
        color: Color.fromRGBO(255, 255, 255, widget.opacity),
        //Color(0xFFFFFFFF),
        width: 48,
        height: 48,
        child: IconButton(
          color: Color(0xFFFFFFFF),
          icon: Icon(
            widget.icon,
            color: Colors.black,
          ),
          onPressed: () {
            setState(() {
              widget.onPress();
            });
          },
        ),
      ),
    );
  }
}
