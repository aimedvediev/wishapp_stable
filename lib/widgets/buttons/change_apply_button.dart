import 'package:flutter/material.dart';
import 'package:wish_app_ver_2/screens/login_registration/boofer_screen.dart';

class ChangeApplyButton extends StatelessWidget {
  final String text;
  final Function() onPressed;

  ChangeApplyButton({
    this.text,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Padding(
        padding: const EdgeInsets.only(top: 18, right: 20),
        child: FlatButton(
          padding: EdgeInsets.all(0),
          onPressed: () {
            onPressed();
            BufferScreen();
          },
          child: Container(
            alignment: Alignment.center,
            height: 64,
            width: 133,
            decoration: new BoxDecoration(
              color: Color(0xFFDC6C54),
              borderRadius: new BorderRadius.circular(8.0),
              boxShadow: [
                new BoxShadow(
                  color: Color.fromRGBO(60, 48, 74, 0.2),
                  offset: new Offset(0, 8),
                  blurRadius: 24,
                )
              ],
            ),
            child: Text(
              text,
              style: TextStyle(
                color: Color(0xFFFFFFFF),
                letterSpacing: 1.25,
                fontFamily: 'OpenSans',
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
