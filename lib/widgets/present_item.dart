import 'package:flutter/material.dart';
import 'package:wish_app_ver_2/my_present_repository.dart';
import 'package:wish_app_ver_2/present_repository.dart';
import 'package:wish_app_ver_2/screens/present_screen.dart';

import 'package:wish_app_ver_2/widgets/text/date_ofadd_card.dart';
import 'package:wish_app_ver_2/widgets/text/name_of_present_card.dart';

import 'image_badge.dart';
import 'link_icon_badge.dart';

class PresentItem extends StatelessWidget {
  final GestureTapCallback onTap;
  final Present present;
  final MyPresent myPresent;
  final String memberPhoto;

  PresentItem(
      {Key key,
      @required this.onTap,
      this.myPresent,
      this.present,
      this.memberPhoto})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Padding(
      padding: const EdgeInsets.only(
        left: 18,
        top: 20,
        right: 19,
      ),
      child: ClipRRect(
        borderRadius: new BorderRadius.circular(8.0),
        child: Container(
          color: Color(0xFFFFFFFF),
          width: width,
          height: 124,
          child: FlatButton(
            padding: EdgeInsets.all(0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 96,
                      height: 124,
                      child: ClipRRect(
                        borderRadius: new BorderRadius.circular(6.0),
                        child: Image.network(
                          present == null ? myPresent.photo : present.photo,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 14,
                            top: 11,
                          ),
                          child: DateOfAddText(
                              text: present == null
                                  ? myPresent.dateOfAdd
                                  : present.dateOfAdd),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 12,
                            top: 6,
                          ),
                          child: NameOfPresentCard(
                              text: present == null
                                  ? myPresent.name
                                  : present.name),
                        ),
                      ],
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 14, right: 14),
                      child: ImageBadge(image: memberPhoto),
                    ),
//                    Padding(
//                      padding: const EdgeInsets.only(
//                        top: 38,
//                        right: 16,
//                      ),
//                      child: LinkBadge(onPress: () {
//                        print('link');
//                      }),
//                    ),
                  ],
                ),
              ],
            ),
            onPressed: onTap,
          ),
        ),
      ),
    );
  }
}
