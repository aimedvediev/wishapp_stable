import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class CustomDatePicker extends StatefulWidget {
  String text;
  final Function() onSelection;
  bool valid;

  CustomDatePicker({this.text, this.valid, this.onSelection});

  @override
  _CustomDatePickerState createState() => _CustomDatePickerState();
}

class _CustomDatePickerState extends State<CustomDatePicker> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    var now = new DateTime.now();
    var time = DateFormat('d MMMM y, HH:mm').format(now);

    String year;
    String month;
    String day;

    return Padding(
      padding: const EdgeInsets.only(top: 12, right: 32),
      child: ClipRRect(
        borderRadius: new BorderRadius.circular(8.0),
        child: FlatButton(
          padding: EdgeInsets.all(0),
          child: Container(
            padding: EdgeInsets.only(left: 12),
            width: width,
            height: 50,
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              borderRadius: new BorderRadius.circular(8.0),
              border: Border.all(
                width: 1,
                color: !widget.valid ? Colors.red : Color(0xFFF4F5F5),
              ),
              color: Color(0xFFFFFFFF),
            ),
            child: Text(
              widget.text,
              style: widget.text == 'MM/DD/YYYY'
                  ? TextStyle(
                      color: Color(0xFFCACBCC),
                      fontSize: 16,
                      letterSpacing: 0.25,
                      fontFamily: 'Muli',
                    )
                  : TextStyle(
                      color: Color(0xFF423932),
                      fontSize: 16,
                      letterSpacing: 0.25,
                      fontFamily: 'Muli',
                    ),
            ),
          ),
          onPressed: () {
            widget.onSelection();
          },
        ),
      ),
    );
  }
}

String giveMonth(String month) {
  var monthList = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];
  return monthList[int.parse(month) - 1];
}
