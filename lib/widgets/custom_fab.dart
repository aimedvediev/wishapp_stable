import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:wish_app_ver_2/screens/creating_editing_present_screen.dart';
import 'package:wish_app_ver_2/src/models/models.dart';

class CustomFab extends StatefulWidget {
  final String memberId;
  final String memberPhoto;
  final GROUPS groups;

  CustomFab({
    this.memberId,
    this.memberPhoto,
    this.groups,
  });

  @override
  _CustomFabState createState() => _CustomFabState();
}

class _CustomFabState extends State<CustomFab> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 19, right: 8),
      child: Container(
        height: 64,
        width: 64,
        decoration: new BoxDecoration(
          color: Color(0xFFDC6C54),
          borderRadius: new BorderRadius.circular(16),
          boxShadow: [
            new BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.14),
              offset: Offset(0, 16),
              blurRadius: 24,
            )
          ],
        ),
        child: ClipRRect(
          borderRadius: new BorderRadius.circular(16),
          child: FlatButton(
            onPressed: () {
              Navigator.of(context).pushNamed(
                CreatingEditingPresentScreen.routeName,
                arguments: [widget.memberId, widget.memberPhoto, widget.groups.toString()],
              );
            },
            child: SvgPicture.asset('lib/assets/icons/icon_plus.svg'),
          ),
        ),
      ),
    );
  }
}
