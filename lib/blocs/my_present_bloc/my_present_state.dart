import 'package:equatable/equatable.dart';
import 'package:wish_app_ver_2/my_present_repository.dart';

abstract class MyPresentState extends Equatable {
  const MyPresentState();

  @override
  List<Object> get props => [];
}

class MyPresentLoading extends MyPresentState {}

class MyPresentLoaded extends MyPresentState {
  final List<MyPresent> myPresents;

  const MyPresentLoaded([this.myPresents = const []]);

  @override
  List<Object> get props => [myPresents];

  String toString() => "PresentLoaded{presents: $myPresents}";
}
