import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wish_app_ver_2/blocs/my_present_bloc/dart.dart';
import 'package:wish_app_ver_2/my_present_repository.dart';

class MyPresentsBloc extends Bloc<MyPresentEvent, MyPresentState> {
  final MyPresentRepository _myPresentRepository;
  StreamSubscription _myPresentSubscription;

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  MyPresentsBloc({@required MyPresentRepository myPresentRepository})
      : assert(myPresentRepository != null),
        _myPresentRepository = myPresentRepository;

  @override
  MyPresentState get initialState => MyPresentLoading();

  @override
  Stream<MyPresentState> mapEventToState(MyPresentEvent event) async* {
    if (event is LoadMyPresents) {
      yield* _mapLoadPresentsToState(event);
    }
    if (event is AddMyPresent) {
      yield* _mapAddPresentToState(event);
    }
    if (event is UpdateMyPresent) {
      yield* _mapUpdatePresentToState(event);
    }
    if (event is DeletedMyPresent) {
      yield* _mapDeletePresentToState(event);
    }
    if (event is MyPresentsUpdated) {
      yield* _mapPresentsUpdateToState(event);
    }
  }

  Stream<MyPresentState> _mapLoadPresentsToState(LoadMyPresents event) async* {
    _myPresentSubscription?.cancel();
    final FirebaseUser currentUser = await _firebaseAuth.currentUser();
    if (event.phoneNr == '0') {
      print(event.phoneNr);
      _myPresentRepository.myPresent(currentUser.phoneNumber).listen(
            (present) => add(MyPresentsUpdated(present)),
          );
    } else {
      print(event.phoneNr);
      _myPresentRepository.myPresent(event.phoneNr).listen(
            (present) => add(MyPresentsUpdated(present)),
          );
    }
  }

  Stream<MyPresentState> _mapAddPresentToState(AddMyPresent event) async* {
    final FirebaseUser currentUser = await _firebaseAuth.currentUser();
    _myPresentRepository.addNewMyPresent(
        event.myPresent, currentUser.phoneNumber);
  }

  Stream<MyPresentState> _mapUpdatePresentToState(
      UpdateMyPresent event) async* {
    final FirebaseUser currentUser = await _firebaseAuth.currentUser();
    _myPresentRepository.updateMyPresent(
        event.updatedMyPresent, currentUser.phoneNumber);
  }

  Stream<MyPresentState> _mapDeletePresentToState(
      DeletedMyPresent event) async* {
    final FirebaseUser currentUser = await _firebaseAuth.currentUser();
    _myPresentRepository.deleteMyPresent(
        event.myPresent, currentUser.phoneNumber);
  }

  Stream<MyPresentState> _mapPresentsUpdateToState(
      MyPresentsUpdated event) async* {
    yield MyPresentLoaded(event.myPresent);
  }
}
