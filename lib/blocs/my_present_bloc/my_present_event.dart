import 'package:equatable/equatable.dart';
import 'package:wish_app_ver_2/my_present_repository.dart';

abstract class MyPresentEvent extends Equatable {
  const MyPresentEvent();

  @override
  List<Object> get props => [];
}

class LoadMyPresents extends MyPresentEvent {
  final String phoneNr;

  const LoadMyPresents(this.phoneNr);

}

class AddMyPresent extends MyPresentEvent {
  final MyPresent myPresent;

  const AddMyPresent(this.myPresent);

  @override
  List<Object> get props => [myPresent];

  @override
  String toString() => "MyPresentAdded {myPresent: $myPresent}";
}

class MyPresentsEmpty extends MyPresentEvent {}

class UpdateMyPresent extends MyPresentEvent {
  final MyPresent updatedMyPresent;

  const UpdateMyPresent(this.updatedMyPresent);

  @override
  List<Object> get props => [updatedMyPresent];

  @override
  String toString() => "MyPresentUpdate {present: $updatedMyPresent}";
}

class DeletedMyPresent extends MyPresentEvent {
  final MyPresent myPresent;

  const DeletedMyPresent(this.myPresent);

  @override
  List<Object> get props => [myPresent];

  @override
  String toString() => "MyPresentDeleted {present: $myPresent}";
}

class MyPresentsUpdated extends MyPresentEvent {
  final List<MyPresent> myPresent;

  const MyPresentsUpdated(this.myPresent);

  @override
  List<Object> get props => [myPresent];
}
