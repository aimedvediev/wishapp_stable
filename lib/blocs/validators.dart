import 'dart:async';

class Validators {
  final validateName = StreamTransformer<String, String>.fromHandlers(
    handleData: (name, sink) {
      if (name.length > 2) {
        sink.add(name);
      } else {
        sink.addError('Name length must be at least 3');
      }
    },
  );

  final validatePhoneNr = StreamTransformer<String, String>.fromHandlers(
    handleData: (phoneNr, sink) {
      if (phoneNr.length > 10) {
        if (phoneNr.contains('+')) {
          sink.add(phoneNr);
        } else {
          sink.addError('Phone Number must start with + and country code');
        }
      } else {
        sink.addError('Phone Nubmer isn\'t correct');
      }
    },
  );

  final validateDescription = StreamTransformer<String, String>.fromHandlers(
    handleData: (description, sink) {
      if (description.length > 4) {
        sink.add(description);
      } else {
        sink.addError('description length must be at least 5');
      }
    },
  );

  static final RegExp _emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );

  static final RegExp _passwordRegExp = RegExp(
    r'^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$',
  );

  static isValidEmail(String email) {
    return _emailRegExp.hasMatch(email);
  }

  static isValidPassword(String password) {
    return _passwordRegExp.hasMatch(password);
  }
}
