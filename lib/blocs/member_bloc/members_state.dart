import 'package:equatable/equatable.dart';
import 'package:wish_app_ver_2/members_repository.dart';

abstract class MembersState extends Equatable {
  const MembersState();

  @override
  List<Object> get props => [];
}

class MembersLoading extends MembersState {}

class MembersLoaded extends MembersState {
  final List<Member> members;

  const MembersLoaded([this.members = const []]);

  @override
  List<Object> get props => [members];

  @override
  String toString() => 'MemberLoaded {members: $members}';
}

