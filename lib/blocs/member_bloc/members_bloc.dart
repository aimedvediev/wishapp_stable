import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

import 'package:wish_app_ver_2/blocs/member_bloc/bloc.dart';
import 'package:wish_app_ver_2/members_repository.dart';

class MembersBloc extends Bloc<MembersEvent, MembersState> {
  final MembersRepository _membersRepository;
  StreamSubscription _memberSubscription;

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  MembersBloc({@required MembersRepository membersRepository})
      : assert(membersRepository != null),
        _membersRepository = membersRepository;

  @override
  MembersState get initialState => MembersLoading();

  @override
  Stream<MembersState> mapEventToState(MembersEvent event) async* {
    if (event is LoadMembers) {
      yield* _mapLoadMembersToState();
    }
    if (event is AddMember) {
      yield* _mapAddMemberToState(event);
    }
    if (event is UpdateMember) {
      yield* _mapUpdateMemberToState(event);
    }
    if (event is DeleteMember) {
      yield* _mapDeleteMemberToState(event);
    }
    if (event is MembersUpdated) {
      yield* _mapMembersUpdateToState(event);
    }
  }

  Stream<MembersState> _mapLoadMembersToState() async* {
    _memberSubscription?.cancel();
    final FirebaseUser currentUser = await _firebaseAuth.currentUser();



    _memberSubscription =
        _membersRepository.member(currentUser.phoneNumber).listen(
              (member) => add(MembersUpdated(member)),
            );
  }

  Stream<MembersState> _mapAddMemberToState(AddMember event) async* {
    final FirebaseUser currentUser = await _firebaseAuth.currentUser();

    _membersRepository.addNewMember(event.member, currentUser.phoneNumber);
  }

  Stream<MembersState> _mapUpdateMemberToState(UpdateMember event) async* {
    final FirebaseUser currentUser = await _firebaseAuth.currentUser();
    _membersRepository.updateMember(
        event.updatedMember, currentUser.phoneNumber);
  }

  Stream<MembersState> _mapDeleteMemberToState(DeleteMember event) async* {
    final FirebaseUser currentUser = await _firebaseAuth.currentUser();
    _membersRepository.deleteMember(event.member, currentUser.phoneNumber);
  }

  Stream<MembersState> _mapMembersUpdateToState(MembersUpdated event) async* {
    yield MembersLoaded(event.members);
  }

  Future<void> close() {
    _memberSubscription?.cancel();
    return super.close();
  }
}
