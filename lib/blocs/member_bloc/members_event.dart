import 'package:equatable/equatable.dart';
import 'package:wish_app_ver_2/members_repository.dart';

abstract class MembersEvent extends Equatable {
  const MembersEvent();

  @override
  List<Object> get props => [];
}

class LoadMembers extends MembersEvent {}

class AddMember extends MembersEvent {
  final Member member;

  const AddMember(this.member);

  @override
  List<Object> get props => [member];

  @override
  String toString() => 'MemberAdded {member: $member}';
}

class MembersEmpty extends MembersEvent {}

class UpdateMember extends MembersEvent {
  final Member updatedMember;

  const UpdateMember(this.updatedMember);

  @override
  List<Object> get props => [updatedMember];

  @override
  String toString() => 'MemberUpdate {member: $updatedMember}';
}

class DeleteMember extends MembersEvent {
  final Member member;

  const DeleteMember(this.member);

  @override
  List<Object> get props => [member];

  @override
  String toString() => 'MemberDeleted {member: $member}';
}

class MembersUpdated extends MembersEvent {
  final List<Member> members;

  const MembersUpdated(this.members);

  @override
  List<Object> get props => [members];
}
