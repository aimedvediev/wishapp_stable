import 'package:equatable/equatable.dart';
import 'package:wish_app_ver_2/present_repository.dart';

abstract class PresentEvent extends Equatable {
  const PresentEvent();

  @override
  List<Object> get props => [];
}

class LoadPresents extends PresentEvent {
}

class AddPresent extends PresentEvent {
  final Present present;

  const AddPresent(this.present);

  @override
  List<Object> get props => [present];

  @override
  String toString() => "PresentAdded {present: $present}";
}

class PresentsEmpty extends PresentEvent {}

class UpdatePresent extends PresentEvent {
  final Present updatedPresent;

  const UpdatePresent(this.updatedPresent);

  @override
  List<Object> get props => [updatedPresent];

  @override
  String toString() => "PresentUpdate {present: $updatedPresent}";
}

class DeletedPresent extends PresentEvent {
  final Present present;

  const DeletedPresent(this.present);

  @override
  List<Object> get props => [present];

  @override
  String toString() => "PresentDeleted {present: $present}";
}

class PresentsUpdated extends PresentEvent {
  final List<Present> present;

  const PresentsUpdated(this.present);

  @override
  List<Object> get props => [present];
}
