import 'package:equatable/equatable.dart';
import 'package:wish_app_ver_2/present_repository.dart';

abstract class PresentState extends Equatable {
const PresentState();

@override
List<Object> get props => [];
}

class PresentLoading extends PresentState {}

class PresentLoaded extends PresentState {
  final List<Present> presents;

  const PresentLoaded([this.presents = const []]);

  @override
  List<Object> get props => [presents];

  String toString() => "PresentLoaded{presents: $presents}";
}
