import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wish_app_ver_2/blocs/present_bloc/bloc.dart';
import 'package:wish_app_ver_2/present_repository.dart';

class PresentsBloc extends Bloc<PresentEvent, PresentState> {
  final PresentRepository _presentRepository;
  StreamSubscription _presentSubscription;

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  PresentsBloc({@required PresentRepository presentRepository})
      : assert(presentRepository != null),
        _presentRepository = presentRepository;

  @override
  PresentState get initialState => PresentLoading();

  @override
  Stream<PresentState> mapEventToState(PresentEvent event) async* {
    if (event is LoadPresents) {
      yield* _mapLoadPresentsToState();
    }
    if (event is AddPresent) {
      yield* _mapAddPresentToState(event);
    }
    if (event is UpdatePresent) {
      yield* _mapUpdatePresentToState(event);
    }
    if (event is DeletedPresent) {
      yield* _mapDeletePresentToState(event);
    }
    if (event is PresentsUpdated) {
      yield* _mapPresentsUpdateToState(event);
    }
  }

  Stream<PresentState> _mapLoadPresentsToState() async* {
    _presentSubscription?.cancel();
    final FirebaseUser currentUser = await _firebaseAuth.currentUser();
    _presentRepository.present(currentUser.phoneNumber).listen(
          (present) => add(PresentsUpdated(present)),
        );
  }

  Stream<PresentState> _mapAddPresentToState(AddPresent event) async* {
    final FirebaseUser currentUser = await _firebaseAuth.currentUser();
    _presentRepository.addNewPresent(event.present, currentUser.phoneNumber);
  }

  Stream<PresentState> _mapUpdatePresentToState(UpdatePresent event) async* {
    final FirebaseUser currentUser = await _firebaseAuth.currentUser();
    _presentRepository.updatePresent(
        event.updatedPresent, currentUser.phoneNumber);
  }

  Stream<PresentState> _mapDeletePresentToState(DeletedPresent event) async* {
    final FirebaseUser currentUser = await _firebaseAuth.currentUser();
    _presentRepository.deletePresent(event.present, currentUser.phoneNumber);
  }

  Stream<PresentState> _mapPresentsUpdateToState(PresentsUpdated event) async* {
    yield PresentLoaded(event.present);
  }
}
