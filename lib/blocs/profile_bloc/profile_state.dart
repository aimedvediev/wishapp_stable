import 'package:equatable/equatable.dart';
import 'package:wish_app_ver_2/members_repository.dart';

abstract class ProfileState extends Equatable {
  const ProfileState();

  @override
  List<Object> get props => [];
}

class ProfileLoading extends ProfileState {}

class ProfileLoaded extends ProfileState {
  final List<Profile> profile;

  const ProfileLoaded([this.profile = const []]);

  @override
  List<Object> get props => [profile];

  @override
  String toString() => 'ProfileLoaded {profile: $profile}';
}

class ProfileEmpty extends ProfileState {}
