import 'package:equatable/equatable.dart';
import 'package:wish_app_ver_2/src/models/models.dart';

abstract class ProfileEvent extends Equatable {
  const ProfileEvent();

  List<Object> get props => [];
}

class LoadProfile extends ProfileEvent {}

class AddProfile extends ProfileEvent {
  final Profile profile;

  const AddProfile(this.profile);

  @override
  List<Object> get props => [profile];

  @override
  String toString() => 'AddProfile {member: $profile}';
}

class UpdateProfile extends ProfileEvent {
  final Profile updatedProfile;

  const UpdateProfile(this.updatedProfile);

  @override
  List<Object> get props => [updatedProfile];

  @override
  String toString() => 'UpdateProfile {member: $updatedProfile}';
}

class DeleteProfile extends ProfileEvent {
  final Profile profile;

  const DeleteProfile(this.profile);

  @override
  List<Object> get props => [profile];

  @override
  String toString() => 'DeleteProfile {member: $profile}';
}

class ProfilesUpdated extends ProfileEvent {
  final List<Profile> profile;

  const ProfilesUpdated(this.profile);

  @override
  List<Object> get props => [profile];
}
