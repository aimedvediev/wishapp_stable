import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

import 'package:wish_app_ver_2/blocs/profile_bloc/bloc.dart';
import 'package:wish_app_ver_2/profile_repository.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final ProfileRepository _profileRepository;
  StreamSubscription _profileSubscription;

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  ProfileBloc({@required ProfileRepository profileRepository})
      : assert(profileRepository != null),
        _profileRepository = profileRepository;

  @override
  ProfileState get initialState => ProfileLoading();

  @override
  Stream<ProfileState> mapEventToState(ProfileEvent event) async* {
    if (event is LoadProfile) {
      yield* _mapLoadProfileToState(event);
    }
    if (event is AddProfile) {
      yield* _mapAddProfileToState(event);
    }
    if (event is UpdateProfile) {
      yield* _mapUpdateProfileToState(event);
    }
    if (event is DeleteProfile) {
      yield* _mapDeleteProfileToState(event);
    }
    if (event is ProfilesUpdated) {
      yield* _mapProfileUpdateToState(event);
    }
    if (event is ProfileEmpty) {}
  }

  Stream<ProfileState> _mapLoadProfileToState(ProfileEvent event) async* {
    _profileSubscription?.cancel();
    FirebaseUser currentUser = await _firebaseAuth.currentUser();
    print(currentUser);
    print(currentUser.phoneNumber);
    _profileSubscription =
        _profileRepository.profile(currentUser.phoneNumber).listen(
              (profile) => add(ProfilesUpdated(profile)),
            );
  }

  Stream<ProfileState> _mapAddProfileToState(AddProfile event) async* {
    FirebaseUser currentUser = await _firebaseAuth.currentUser();
    _profileRepository.addNewProfile(event.profile, currentUser.phoneNumber);
  }

  Stream<ProfileState> _mapUpdateProfileToState(UpdateProfile event) async* {
    FirebaseUser currentUser = await _firebaseAuth.currentUser();
    _profileRepository.updateProfile(
        event.updatedProfile, currentUser.phoneNumber);
  }

  Stream<ProfileState> _mapDeleteProfileToState(DeleteProfile event) async* {
    _profileRepository.deleteProfile(event.profile);
  }

  Stream<ProfileState> _mapProfileUpdateToState(ProfilesUpdated event) async* {
    if (event.profile.length == 0) {
      yield ProfileEmpty();
    } else {
      yield ProfileLoaded(event.profile);
    }
  }

  Future<void> close() {
    _profileSubscription?.cancel();
    return super.close();
  }
}
