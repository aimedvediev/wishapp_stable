import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:wish_app_ver_2/members_repository.dart';
import 'package:wish_app_ver_2/services/cloud_storage_result.dart';
import 'package:wish_app_ver_2/widgets/buttons/change_apply_button.dart';
import 'package:wish_app_ver_2/widgets/custom_date_picker/date_picker.dart';
import 'package:wish_app_ver_2/widgets/filterChips/chip_filter.dart';
import 'package:wish_app_ver_2/widgets/photo_picker/image_selector.dart';
import 'package:wish_app_ver_2/widgets/text/text_field.dart';
import 'package:wish_app_ver_2/widgets/text/title_grey_text.dart';

typedef OnSaveCallback = Function(
  String name,
  String phoneNr,
  GROUPS groups,
  String photo,
  String dateOfBD,
);

class CreatingEditingMemberScreen extends StatefulWidget {
  static const routeName = '/creating_editing_member_screen';
  final OnSaveCallback onSave;
  final Member member;
  final bool isEditing;

  CreatingEditingMemberScreen({Key key, @required this.onSave, this.member, this.isEditing})
      : super(key: key);

  @override
  _CreatingEditingMemberScreenState createState() => _CreatingEditingMemberScreenState();
}

class _CreatingEditingMemberScreenState extends State<CreatingEditingMemberScreen> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _name;

  String _phoneNr;

  GROUPS _groups = GROUPS.FAMILY;
  String _photo;

  String _dateOfBD = 'MM/DD/YYYY';

  bool validDate = true;
  bool validPhoto = true;
  bool uploadPhoto = false;

  List<GROUPS> groupsList = [
    GROUPS.FAMILY,
    GROUPS.FRIENDS,
    GROUPS.COLLEAGUES,
  ];

  GROUPS selectedGroup = GROUPS.FAMILY;

  File _selectedImage;

  bool get isEditing => widget.isEditing;

  StorageUploadTask uploadTask;

  Future<CloudStorageResult> uploadImage({
    @required File imageToUpload,
    @required String title,
  }) async {
    setState(() {
      uploadPhoto = true;
    });
    var imageFileName = title + DateTime.now().millisecondsSinceEpoch.toString();
//todo profile/phoneNr/member or present/imageFileName
    final StorageReference firebaseStorageRef = FirebaseStorage.instance.ref().child(imageFileName);

    if (widget.member != null) {
      FirebaseStorage.instance
          .getReferenceFromUrl(widget.member.photo)
          .then((value) => value.delete());
    }

    uploadTask = firebaseStorageRef.putFile(imageToUpload);
    StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;

    var downloadUrl = await storageTaskSnapshot.ref.getDownloadURL();

    if (uploadTask.isComplete) {
      var url = downloadUrl.toString();
      return CloudStorageResult(
        imageUrl: url,
        imageFileName: imageFileName,
      );
    }
    return null;
  }

  Future selectImage(File image) async {
    _selectedImage = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      image = _selectedImage;
      validPhoto = true;
    });
  }

  @override
  void initState() {
    isEditing ? _dateOfBD = widget.member.dateOfBD : _dateOfBD = _dateOfBD;
    isEditing ? selectedGroup = widget.member.group : selectedGroup = selectedGroup;
    isEditing ? _groups = widget.member.group : _groups = _groups;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Color(0xFFFFFFFF),
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xFFFFFFFF),
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        title: TitleGreyText(text: isEditing ? 'Edit Member Profile' : 'Create Member Profile'),
      ),
      body: ClipRRect(
        borderRadius: new BorderRadius.circular(16.0),
        child: Container(
          alignment: Alignment.topLeft,
          padding: const EdgeInsets.only(left: 20, top: 27),
          width: width,
          color: Color(0xFFFAFAFA),
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  TextTitleField(text: 'Member photo'),
                  //TODO picker for photo and edit photo badge
                  Padding(
                    padding: const EdgeInsets.only(top: 12),
                    child: ImageSelector(
                      onSelection: () {
                        selectImage(_selectedImage);
                      },
                      image: _selectedImage,
                      valid: validPhoto,
                      isEditing: isEditing,
                      oldImage: isEditing ? widget.member.photo : '',
                    ),
                  ),
                  TextTitleField(text: 'Name Member'),
                  Padding(
                    padding: const EdgeInsets.only(top: 12, right: 32),
                    child: Container(
                      height: 50,
                      padding: EdgeInsets.only(left: 12),
                      decoration: BoxDecoration(
                        borderRadius: new BorderRadius.circular(8.0),
                        border: Border.all(
                          width: 1,
                          color: Color(0xFFF4F5F5),
                        ),
                        color: Color(0xFFFFFFFF),
                      ),
                      child: TextFormField(
                        initialValue: isEditing ? widget.member.name : '',
                        style: TextStyle(
                          color: Color(0xFF423932),
                          fontSize: 16,
                          letterSpacing: 0.25,
                          fontFamily: 'Muli',
                        ),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Type Name',
                          hintStyle: TextStyle(
                            color: Color(0xFFCACBCC),
                            fontSize: 16,
                            letterSpacing: 0.25,
                            fontFamily: 'Muli',
                          ),
                        ),
                        validator: (value) {
                          if (value.length < 2) {
                            return 'Name must have at least 3 symbols';
                          }
                          return null;
                        },
                        onSaved: (value) => _name = value,
                      ),
                    ),
                  ),
                  TextTitleField(text: 'Phone Number'),
                  Padding(
                    padding: const EdgeInsets.only(top: 12, right: 32),
                    child: Container(
                      height: 50,
                      padding: EdgeInsets.only(left: 12),
                      decoration: BoxDecoration(
                        borderRadius: new BorderRadius.circular(8.0),
                        border: Border.all(
                          width: 1,
                          color: Color(0xFFF4F5F5),
                        ),
                        color: Color(0xFFFFFFFF),
                      ),
                      child: TextFormField(
                        initialValue: isEditing ? widget.member.phoneNr : '',
                        style: TextStyle(
                          color: Color(0xFF423932),
                          fontSize: 16,
                          letterSpacing: 0.25,
                          fontFamily: 'Muli',
                        ),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Type Phone',
                          hintStyle: TextStyle(
                            color: Color(0xFFCACBCC),
                            fontSize: 16,
                            letterSpacing: 0.25,
                            fontFamily: 'Muli',
                          ),
                        ),
                        onSaved: (value) => _phoneNr = value,
                        validator: (value) {
                          Pattern pattern = r'^\+[1-9]{1}[0-9]{10,14}$';
                          RegExp regex = new RegExp(pattern);
                          if (!regex.hasMatch(value)) {
                            return 'Invalid mobile number';
                          }
                          return null;
                        },
                      ),
                    ),
                  ),
                  TextTitleField(text: 'Date of Birth'),
                  CustomDatePicker(
                    text: _dateOfBD,
                    valid: validDate,
                    onSelection: () {
                      DatePicker.showDatePicker(context,
                          showTitleActions: true,
                          minTime: DateTime(1900, 1, 1),
                          maxTime: DateTime.now(), onConfirm: (date) {
                        var day = date.day.toString();
                        var month = date.month.toString();
                        var year = date.year.toString();
                        setState(() {
                          validDate = true;
                          _dateOfBD = '$day ${giveMonth(month)} $year';
                        });
                      }, locale: LocaleType.en);
                    },
                  ),
                  TextTitleField(text: 'Choose category'),
                  //TODO fix left padding
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: ChipsFilter(
                      selectedGroup: selectedGroup,
                      groups: groupsList,
                      onSelectionChip: (selected) {
                        setState(
                          () {
                            _groups = selected;
                            selectedGroup = selected;
                          },
                        );
                      },
                    ),
                  ),
                  uploadPhoto
                      ? Center(
                          child: Container(
                            alignment: Alignment.center,
                            height: 64,
                            width: 133,
                            child: CircularProgressIndicator(),
                          ),
                        )
                      : ChangeApplyButton(
                          text: isEditing ? 'CHANGE' : 'CREATE',
                          onPressed: () async {
                            if (_dateOfBD == 'MM/DD/YYYY') {
                              setState(() {
                                validDate = false;
                              });
                            }
                            if (_selectedImage == null) {
                              setState(() {
                                validPhoto = false;
                              });
                            }
                            _formKey.currentState.save();
                            if (_formKey.currentState.validate() && validDate) {
                              if (_selectedImage != null) {
                                CloudStorageResult storageResult;
                                storageResult = await uploadImage(
                                    imageToUpload: _selectedImage, title: _phoneNr);
                                _photo = storageResult.imageUrl;
                              } else {
                                _photo = widget.member.photo;
                              }

                              await widget.onSave(_name, _phoneNr, _groups, _photo, _dateOfBD);
                              Navigator.pop(context);
                            }
                          },
                        ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
