import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:focused_menu/focused_menu.dart';
import 'package:focused_menu/modals.dart';
import 'package:wish_app_ver_2/blocs/present_bloc/bloc.dart';
import 'package:wish_app_ver_2/members_repository.dart';
import 'package:wish_app_ver_2/screens/present_screen.dart';
import 'package:wish_app_ver_2/widgets/custom_fab.dart';

import 'package:wish_app_ver_2/widgets/filterChips/chip_filter.dart';
import 'package:wish_app_ver_2/widgets/present_item.dart';
import 'package:wish_app_ver_2/widgets/text/main_text.dart';
import 'package:wish_app_ver_2/widgets/text/text_comma.dart';

class MyPresentsScreen extends StatefulWidget {
  @override
  _MyPresentsScreenState createState() => _MyPresentsScreenState();
}

class _MyPresentsScreenState extends State<MyPresentsScreen> {
  List<GROUPS> groupsList = [
    GROUPS.ALL,
    GROUPS.FAMILY,
    GROUPS.FRIENDS,
    GROUPS.COLLEAGUES,
  ];

  GROUPS selectedGroup = GROUPS.ALL;

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<PresentsBloc>(context).add(LoadPresents());

    double height = MediaQuery.of(context).size.height;
    var padding = MediaQuery.of(context).padding;

    return Scaffold(
      floatingActionButton: CustomFab(
        memberId: '0',
      ),
      backgroundColor: Color(0xffFAFAFA),
      body: Container(
        padding: (EdgeInsets.only(top: 45)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  TextComma('My'),
                  MainText("Ideas for presents"),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 18, left: 10),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    ChipsFilter(
                      selectedGroup: selectedGroup,
                      groups: groupsList,
                      onSelectionChip: (selected) {
                        setState(
                          () {
                            selectedGroup = selected;
                          },
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: Container(
                height: height -
                    padding.top -
                    padding.bottom -
                    kToolbarHeight -
                    120,
                child: BlocBuilder<PresentsBloc, PresentState>(
                  //todo clean it
                  builder: (context, state) {
                    if (state is PresentLoading) {
                      return CircularProgressIndicator();
                    } else if (state is PresentLoaded) {
                      if (selectedGroup == GROUPS.ALL) {
                        final presents = state.presents;
                        return ListView.builder(
                          padding: EdgeInsets.only(top: 3),
                          itemCount: presents.length,
                          itemBuilder: (context, index) {
                            final present = presents[index];
                            return FocusedMenuHolder(
                              menuOffset: 20,
                              menuWidth: MediaQuery.of(context).size.width*0.50,
                              blurBackgroundColor: Color(0xFFFAFAFA),
                              menuItems: <FocusedMenuItem>[
                                FocusedMenuItem(
                                    title: Text(
                                      "Delete",
                                      style: TextStyle(color: Colors.redAccent),
                                    ),
                                    trailingIcon: Icon(
                                      Icons.delete,
                                      color: Colors.redAccent,
                                    ),
                                    onPressed: () {
                                      _showMyDialog(present);
                                    }),
                                FocusedMenuItem(
                                    title: Text(
                                      "Recived",
                                      style:
                                          TextStyle(color: Colors.greenAccent),
                                    ),
                                    trailingIcon: Icon(
                                      Icons.add_shopping_cart,
                                      color: Colors.greenAccent,
                                    ),
                                    onPressed: () {
                                      _showMyDialog(present);
                                    }),
                              ],
                              child: PresentItem(
                                memberPhoto: present.memberPhoto,
                                present: present,
                                onTap: () async {
                                  final removedMember =
                                      await Navigator.of(context).push(
                                    MaterialPageRoute(builder: (_) {
                                      return PresentScreen(
                                        presentId: present.id,
                                      );
                                    }),
                                  );
                                },
                              ),
                            );
                          },
                        );
                      } else {
                        final presents = state.presents
                            .where((present) => present.groups == selectedGroup)
                            .toList();

                        return ListView.builder(
                          padding: EdgeInsets.only(top: 3),
                          itemCount: presents.length,
                          itemBuilder: (context, index) {
                            final present = presents[index];
                            return FocusedMenuHolder(
                              blurBackgroundColor: Color(0xFFFAFAFA),
                              menuOffset: 20,
                              menuWidth: MediaQuery.of(context).size.width*0.50,
                              menuItems: <FocusedMenuItem>[
                                FocusedMenuItem(
                                    title: Text(
                                      "Delete",
                                      style: TextStyle(color: Colors.redAccent),
                                    ),
                                    trailingIcon: Icon(
                                      Icons.delete,
                                      color: Colors.redAccent,
                                    ),
                                    onPressed: () {
                                      _showMyDialog(present);
                                    }),
                                FocusedMenuItem(
                                    title: Text(
                                      "Received",
                                      style:
                                          TextStyle(color: Color(0xFF8CD996)),
                                    ),
                                    trailingIcon: Icon(
                                      Icons.add_shopping_cart,
                                      color: Color(0xFF8CD996),
                                    ),
                                    onPressed: () {}),
                              ],
                              child: PresentItem(
                                memberPhoto: present.memberPhoto,
                                present: present,
                                onTap: () async {
                                  final removedMember =
                                      await Navigator.of(context).push(
                                    MaterialPageRoute(builder: (_) {
                                      return PresentScreen(
                                        presentId: present.id,
                                      );
                                    }),
                                  );
                                },
                              ),
                            );
                          },
                        );
                      }
                    } else {
                      return Container();
                    }
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _showMyDialog(Present present) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Delete ${present.name}'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Would you like to delete present'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Yes'),
              onPressed: () {
                BlocProvider.of<PresentsBloc>(context)
                    .add(DeletedPresent(present));
                Navigator.pop(context, present);
              },
            ),
          ],
        );
      },
    );
  }
}
