import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wish_app_ver_2/widgets/text/main_text.dart';
import 'package:wish_app_ver_2/widgets/text/text_comma.dart';
import 'package:wish_app_ver_2/blocs/authentication_block/bloc.dart';

import 'member_screen.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Color(0xffFAFAFA),
      body: Container(
        padding: (EdgeInsets.only(top: 45)),
        child: Padding(
          padding: const EdgeInsets.only(left: 18),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        TextComma('Hi,'),
                        MainText('todo'),
                      ],
                    ),
                    IconButton(
                      icon: Icon(Icons.exit_to_app),
                      onPressed: () {
//                        BlocProvider.of<AuthenticationBloc>(context).add(
//                          LoggedOut(),
//                        );
                      },
                    ),
                    ClipRRect(
                      borderRadius: new BorderRadius.circular(25),
                      child: Container(
                        height: 52,
                        width: 52,
                        child: FlatButton(
                          padding: EdgeInsets.all(0),
                          onPressed: () => Navigator.of(context).pushNamed(
                            MemberScreen.routeName,
                            arguments: '0',
                          ),
                          child: Image.network(
                            'todo',
                            fit: BoxFit.cover,
                            height: 52,
                            width: 52,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              //CALENDAR
              Padding(
                padding: const EdgeInsets.only(top: 24),
                child: Container(
                  height: 86,
                  //todo can be some problems with shadow
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: 5,
                    itemBuilder: (BuildContext context, int index) {
                      //todo normal data
                      return Padding(
                        padding: const EdgeInsets.only(right: 16),
                        child: Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(top: 14),
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            borderRadius: new BorderRadius.circular(8.0),
                            boxShadow: [
                              new BoxShadow(
                                  color: Color.fromRGBO(52, 48, 45, 0.06),
                                  offset: new Offset(0, 8),
                                  blurRadius: 24)
                            ],
                          ),
                          height: 86,
                          width: 74,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                'SUN',
                                style: TextStyle(
                                    color: Color(0xff423932),
                                    fontFamily: 'OpenSans',
                                    fontSize: 10,
                                    letterSpacing: 0.5),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 6),
                                child: Text(
                                  '12',
                                  style: TextStyle(
                                    color: Color(0xFF423932),
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'OpenSans',
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
              //TODAY
              Padding(
                padding: const EdgeInsets.only(top: 22),
                child: Text(
                  'Today',
                  style: TextStyle(
                    color: Color(0xFFE0E0E0),
                    fontSize: 22,
                    fontFamily: 'Muli',
                  ),
                ),
              ),
              //TodayCard
              Padding(
                padding: const EdgeInsets.only(top: 18, right: 20),
                child: Container(
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    borderRadius: new BorderRadius.circular(8.0),
                    boxShadow: [
                      new BoxShadow(
                          color: Color.fromRGBO(187, 195, 201, 0.2),
                          offset: new Offset(0, 8),
                          blurRadius: 64)
                    ],
                  ),
                  height: 136,
                  width: width,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 24, right: 24),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              '12 April',
                              style: TextStyle(
                                color: Color(0xFFDC6C54),
                                fontSize: 22,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'OpenSans',
                              ),
                            ),
                            Text(
                              'Easter',
                              style: TextStyle(
                                  color: Color(0xFF4F4F4F),
                                  fontSize: 16,
                                  fontFamily: 'OpenSans'),
                            ),
                          ],
                        ),
                        Container(
                          height: 104,
                          width: 104,
                          child: Image.network(
                            'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR-VN4o5fF26h4FuLicMI05UBHMPz6Xz4IhmsGplfkmbIJ5qToD&usqp=CAU',
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              //UPCOMING
              Padding(
                padding: const EdgeInsets.only(top: 22),
                child: Text(
                  'Upcoming',
                  style: TextStyle(
                    color: Color(0xFFE0E0E0),
                    fontSize: 22,
                    fontFamily: 'Muli',
                  ),
                ),
              ),
              //CALENDAR
              Padding(
                padding: const EdgeInsets.only(top: 15),
                child: Container(
                  height: 86,
                  //todo can be some problems with shadow
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: 5,
                    itemBuilder: (BuildContext context, int index) {
                      //todo normal data
                      return Padding(
                        padding: const EdgeInsets.only(right: 16),
                        child: Container(
                          //padding: EdgeInsets.only(top: 14),
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            borderRadius: new BorderRadius.circular(8.0),
                            boxShadow: [
                              new BoxShadow(
                                  color: Color.fromRGBO(52, 48, 45, 0.06),
                                  offset: new Offset(0, 8),
                                  blurRadius: 24)
                            ],
                          ),
                          height: 84,
                          width: 211,
                          child: Padding(
                            padding: const EdgeInsets.only(
                                top: 16, left: 16, right: 16),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      '15 December',
                                      style: TextStyle(
                                        color: Color(0xffDC6C54),
                                        fontFamily: 'OpenSans',
                                        fontSize: 16,
                                        letterSpacing: 0.5,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 2),
                                      child: Text(
                                        'Birthday',
                                        style: TextStyle(
                                          color: Color(0xFF423932),
                                          fontSize: 12,
                                          fontFamily: 'OpenSans',
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    ClipRRect(
                                      borderRadius:
                                          new BorderRadius.circular(25.0),
                                      child: Container(
                                        height: 28,
                                        width: 28,
                                        child: Image.network(
                                            'https://scontent-waw1-1.xx.fbcdn.net/v/t1.0-9/70085316_2418975705037241_3957083172306419712_o.jpg?_nc_cat=1&_nc_sid=85a577&_nc_ohc=MaRQHSCtqDYAX98jDyB&_nc_ht=scontent-waw1-1.xx&oh=f2fa143b23695431bc55dccf46b0717b&oe=5EC8528E'),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 8),
                                      child: Text(
                                        'in 32 days',
                                        style: TextStyle(
                                          color: Color(0xFFBDBDBD),
                                          fontSize: 10,
                                          letterSpacing: 0.2,
                                          fontFamily: 'OpenSans',
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
              //LASTUPDATE
              Padding(
                padding: const EdgeInsets.only(top: 22),
                child: Text(
                  'Last updates',
                  style: TextStyle(
                    color: Color(0xFFE0E0E0),
                    fontSize: 22,
                    fontFamily: 'Muli',
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
