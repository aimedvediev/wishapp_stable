import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wish_app_ver_2/blocs/my_present_bloc/dart.dart';
import 'package:wish_app_ver_2/screens/creating_editing_present_screen.dart';
import 'package:wish_app_ver_2/screens/editing_myPresent_screen.dart';

import 'package:wish_app_ver_2/widgets/buttons/small_button.dart';
import 'package:wish_app_ver_2/widgets/text/date_ofadd_card.dart';
import 'package:wish_app_ver_2/widgets/text/main_text.dart';

class MyPresentScreen extends StatelessWidget {
  static const routeName = '/present_screen';

  final String presentId;
  String memberPhone;

  MyPresentScreen({Key key, this.presentId, this.memberPhone})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    imageCache.clear();
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    var loadedPresent;

    BlocProvider.of<MyPresentsBloc>(context)
        .add(LoadMyPresents(memberPhone == null ? '0' : memberPhone));

    return BlocBuilder<MyPresentsBloc, MyPresentState>(
        builder: (context, state) {
      if (state is MyPresentLoading) {
        return SliverToBoxAdapter(
          child: CircularProgressIndicator(),
        );
      } else if (state is MyPresentLoaded) {
        loadedPresent = state.myPresents.firstWhere(
            (present) => present.id == presentId,
            orElse: () => null);
      }

      Future<void> _showMyDialog() async {
        return showDialog<void>(
          context: context,
          barrierDismissible: false, // user must tap button!
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Delete ${loadedPresent.name}'),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Text('Would you like to delete present'),
                  ],
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('No'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  child: Text('Yes'),
                  onPressed: () {
                    BlocProvider.of<MyPresentsBloc>(context)
                        .add(DeletedMyPresent(loadedPresent));
                    Navigator.pop(context, loadedPresent);
                  },
                ),
              ],
            );
          },
        );
      }

      return Scaffold(
        body: ClipRRect(
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: ResizeImage(
                  NetworkImage(
                    loadedPresent.photo,
                  ),
                  height: (height / 2).round(),
                  width: width.round(),
                ),
                fit: BoxFit.contain,
                alignment: Alignment.topCenter,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: height / 2 - 25,
                  alignment: Alignment(-1, -1),
                  padding: (EdgeInsets.only(top: 45)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 18),
                        child: SmallButton(
                          opacity: 0.7,
                          icon: Icons.keyboard_arrow_down,
                          onPress: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                      memberPhone == null
                          ? Padding(
                              padding: const EdgeInsets.only(right: 19),
                              child: SmallButton(
                                opacity: 0.7,
                                icon: Icons.edit,
                                onPress: () {
                                  Navigator.of(context).push(
                                      MaterialPageRoute(builder: (context) {
                                    return EditingMyPresentScreen(
                                      onSave: (forWhomId,
                                          name,
                                          description,
                                          photo,
                                          dateOfAdd,
                                          link,
                                          groups,
                                          received) {
                                        BlocProvider.of<MyPresentsBloc>(context)
                                            .add(UpdateMyPresent(
                                                loadedPresent.copyWith(
                                                    forWhomId: forWhomId,
                                                    name: name,
                                                    description: description,
                                                    photo: photo,
                                                    dateOfAdd: dateOfAdd,
                                                    link: link,
                                                    groups: groups,
                                                    received: received)));
                                      },
                                      isEditing: true,
                                      myPresent: loadedPresent,
                                    );
                                  }));
                                },
                              ))
                          : Container(
                              height: 0,
                              width: 0,
                            ),
                    ],
                  ),
                ),
                ClipRRect(
                  borderRadius: new BorderRadius.circular(25.0),
                  child: Container(
                    height: height - (height / 2 - 25),
                    color: Colors.white,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 26, left: 18, right: 18),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      MainText(loadedPresent.name),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 8),
                                        child: DateOfAddText(
                                            text: loadedPresent.dateOfAdd),
                                      )
                                    ],
                                  ),
                                  ClipRRect(
                                    borderRadius:
                                        new BorderRadius.circular(25.0),
                                    child: Image.network(
                                      '',
                                      height: 44,
                                      width: 44,
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 16),
                                child: Text(
                                  loadedPresent.description,
                                  style: TextStyle(
                                    color: Color(0xFF655B53),
                                    fontSize: 16,
                                    fontFamily: 'Muli',
                                    letterSpacing: 0.25,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
//                              Container(
//                                padding: EdgeInsets.only(top: 50),
//                                alignment: Alignment.bottomRight,
//                                child: ClipRRect(
//                                  borderRadius: new BorderRadius.circular(7.0),
//                                  child: Container(
//                                    height: 48,
//                                    width: 48,
//                                    color: Color(0xFFFAFAF9),
//                                    child: IconButton(
//                                      padding: EdgeInsets.all(0),
//                                      icon: Icon(Icons.insert_link),
//                                      onPressed: () {},
//                                    ),
//                                  ),
//                                ),
//                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}
