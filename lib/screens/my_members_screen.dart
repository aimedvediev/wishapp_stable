import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:focused_menu/focused_menu.dart';
import 'package:focused_menu/modals.dart';
import 'package:wish_app_ver_2/blocs/member_bloc/bloc.dart';
import 'package:wish_app_ver_2/blocs/present_bloc/bloc.dart';

import 'package:wish_app_ver_2/members_repository.dart';
import 'package:wish_app_ver_2/screens/member_screen.dart';
import 'package:wish_app_ver_2/widgets/filterChips/chip_filter.dart';
import 'package:wish_app_ver_2/widgets/member_screen_member_item.dart';

import 'package:wish_app_ver_2/widgets/text/main_text.dart';
import 'package:wish_app_ver_2/widgets/text/text_comma.dart';

import 'package:wish_app_ver_2/widgets/buttons/small_button.dart';

import 'creating_editing_member_screen.dart';

class MyMembersScreen extends StatefulWidget {
  @override
  _MyMembersScreenState createState() => _MyMembersScreenState();
}

class _MyMembersScreenState extends State<MyMembersScreen> {
  bool isActive = true;

  List<GROUPS> groupsList = [
    GROUPS.ALL,
    GROUPS.FAMILY,
    GROUPS.FRIENDS,
    GROUPS.COLLEAGUES,
  ];

  GROUPS selectedGroup = GROUPS.ALL;

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    var padding = MediaQuery.of(context).padding;

    BlocProvider.of<MembersBloc>(context).add(LoadMembers());

    return Scaffold(
      backgroundColor: Color(0xFFFAFAFA),
      //backgroundColor: Colors.grey,
      body: Container(
        padding: (EdgeInsets.only(top: 45)),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      TextComma('My'),
                      MainText("Members"),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 25),
                  child: SmallButton(
                    opacity: 1,
                    icon: Icons.add,
                    onPress: () {
                      Navigator.of(context)
                          .pushNamed(CreatingEditingMemberScreen.routeName);
                    },
                  ),
                ),
              ],
            ),
            //all filter
            Padding(
              padding: const EdgeInsets.only(top: 18, left: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      ChipsFilter(
                        selectedGroup: selectedGroup,
                        directions: 'vertical',
                        groups: groupsList,
                        onSelectionChip: (selected) {
                          setState(
                            () {
                              selectedGroup = selected;
                            },
                          );
                        },
                      ),
                    ],
                  ),
                  Expanded(
                    child: Container(
                      height: height -
                          padding.top -
                          padding.bottom -
                          kToolbarHeight -
                          120,
                      child: BlocBuilder<MembersBloc, MembersState>(
                        //todo clean it
                        builder: (context, state) {
                          if (state is MembersLoading) {
                            return CircularProgressIndicator();
                          } else if (state is MembersLoaded) {
                            if (selectedGroup == GROUPS.ALL) {
                              final members = state.members;
                              return ListView.builder(
                                padding: EdgeInsets.only(top: 3),
                                itemCount: members.length,
                                itemBuilder: (context, index) {
                                  final member = members[index];
                                  return FocusedMenuHolder(
                                    blurBackgroundColor: Color(0xFFFAFAFA),
                                    menuItems: <FocusedMenuItem>[
                                      FocusedMenuItem(
                                          title: Text(
                                            "Delete",
                                            style: TextStyle(
                                                color: Colors.redAccent),
                                          ),
                                          trailingIcon: Icon(
                                            Icons.delete,
                                            color: Colors.redAccent,
                                          ),
                                          onPressed: () {
                                            _showMyDialog(member);
                                          }),
                                    ],
                                    child: MemberItem(
                                      member: member,
                                      onTap: () async {
                                        final removedMember =
                                            await Navigator.of(context)
                                                .pushNamed(
                                                    MemberScreen.routeName,
                                                    arguments: member.phoneNr);
                                      },
                                    ),
                                  );
                                },
                              );
                            } else {
                              final members = state.members
                                  .where(
                                      (member) => member.group == selectedGroup)
                                  .toList();
                              return ListView.builder(
                                padding: EdgeInsets.only(top: 3),
                                itemCount: members.length,
                                itemBuilder: (context, index) {
                                  final member = members[index];
                                  return FocusedMenuHolder(
                                    blurBackgroundColor: Color(0xFFFAFAFA),
                                    menuItems: <FocusedMenuItem>[
                                      FocusedMenuItem(
                                          title: Text(
                                            "Delete",
                                            style: TextStyle(
                                                color: Colors.redAccent),
                                          ),
                                          trailingIcon: Icon(
                                            Icons.delete,
                                            color: Colors.redAccent,
                                          ),
                                          onPressed: () {
                                            _showMyDialog(member);
                                          })
                                    ],
                                    child: MemberItem(
                                      member: member,
                                      onTap: () async {
                                        final removedMember =
                                            await Navigator.of(context)
                                                .pushNamed(
                                                    MemberScreen.routeName,
                                                    arguments: member.phoneNr);
                                      },
                                    ),
                                  );
                                },
                              );
                            }
                          } else {
                            return Container();
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _showMyDialog(Member member) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Delete ${member.name}'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Would you like to delete member'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Yes'),
              onPressed: () {
                final presents = BlocProvider.of<PresentsBloc>(context).state
                    as PresentLoaded;
                final present = presents.presents
                    .where((present) => present.forWhomId == member.phoneNr)
                    .toList();
                present.forEach((present) =>
                    BlocProvider.of<PresentsBloc>(context)
                        .add(DeletedPresent(present)));
                BlocProvider.of<MembersBloc>(context).add(DeleteMember(member));
                Navigator.pop(context, member);
              },
            ),
          ],
        );
      },
    );
  }
}
