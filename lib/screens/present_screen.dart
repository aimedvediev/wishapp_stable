import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:wish_app_ver_2/blocs/present_bloc/bloc.dart';
import 'package:wish_app_ver_2/screens/creating_editing_present_screen.dart';

import 'package:wish_app_ver_2/widgets/buttons/small_button.dart';
import 'package:wish_app_ver_2/widgets/text/date_ofadd_card.dart';
import 'package:wish_app_ver_2/widgets/text/main_text.dart';

class PresentScreen extends StatelessWidget {
  static const routeName = '/present_screen';

  final String presentId;

  const PresentScreen({Key key, this.presentId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    imageCache.clear();
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return BlocBuilder<PresentsBloc, PresentState>(builder: (context, state) {
      final loadedPresent = (state as PresentLoaded)
          .presents
          .firstWhere((present) => present.id == presentId, orElse: () => null);

      return Scaffold(
        body: loadedPresent == null
            ? Container(child: Text('NO PRESENTS'))
            : ClipRRect(
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: ResizeImage(
                        NetworkImage(loadedPresent.photo),
                        height: (height / 2).round(),
                        width: width.round(),
                      ),
                      alignment: Alignment.topCenter,
                      fit: BoxFit.contain,
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: height / 2 - 25,
                        alignment: Alignment(-1, -1),
                        padding: (EdgeInsets.only(top: 45)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 18),
                              child: SmallButton(
                                opacity: 0.7,
                                icon: Icons.keyboard_arrow_down,
                                onPress: () {
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 19),
                              child: SmallButton(
                                  opacity: 0.7,
                                  icon: Icons.edit,
                                  onPress: () {
                                    Navigator.of(context)
                                        .push(MaterialPageRoute(builder: (context) {
                                      return CreatingEditingPresentScreen(
                                        onSave: (forWhomId, name, description, photo, dateOfAdd,
                                            link, groups, received, memberPhoto) {
                                          BlocProvider.of<PresentsBloc>(context).add(UpdatePresent(
                                              loadedPresent.copyWith(
                                                  forWhomId: forWhomId,
                                                  name: name,
                                                  description: description,
                                                  photo: photo,
                                                  dateOfAdd: dateOfAdd,
                                                  link: link,
                                                  groups: groups,
                                                  received: received)));
                                        },
                                        isEditing: true,
                                        present: loadedPresent,
                                      );
                                    }));
                                  }),
                            ),
                          ],
                        ),
                      ),
                      ClipRRect(
                        borderRadius: new BorderRadius.circular(25.0),
                        child: Container(
                          height: height - (height / 2 - 25),
                          color: Colors.white,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(top: 26, left: 18, right: 18),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            MainText(loadedPresent.name),
                                            Padding(
                                              padding: const EdgeInsets.only(top: 8),
                                              child: DateOfAddText(
                                                  //todo fix
                                                  text:
                                                      'added: ${loadedPresent.dateOfAdd.substring(0, loadedPresent.dateOfAdd.length - 5)}'),
                                            )
                                          ],
                                        ),
                                        ClipRRect(
                                          borderRadius: new BorderRadius.circular(25.0),
                                          child: loadedPresent.memberPhoto == null
                                              ? Container(
                                                  height: 44,
                                                  width: 44,
                                                )
                                              : Image.network(
                                                  //todo fix
                                                  loadedPresent.memberPhoto,
                                                  height: 44,
                                                  width: 44,
                                                ),
                                        ),
                                      ],
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 16),
                                      child: Text(
                                        loadedPresent.description,
                                        style: TextStyle(
                                          color: Color(0xFF655B53),
                                          fontSize: 16,
                                          fontFamily: 'Muli',
                                          letterSpacing: 0.25,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
//                                    Container(
//                                      padding: EdgeInsets.only(top: 50),
//                                      alignment: Alignment.bottomRight,
//                                      child: ClipRRect(
//                                        borderRadius:
//                                            new BorderRadius.circular(7.0),
//                                        child: Container(
//                                          height: 48,
//                                          width: 48,
//                                          color: Color(0xFFFAFAF9),
//                                          child: IconButton(
//                                            padding: EdgeInsets.all(0),
//                                            icon: Icon(Icons.insert_link),
//                                            onPressed: () {},
//                                          ),
//                                        ),
//                                      ),
//                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
      );
    });
  }
}
