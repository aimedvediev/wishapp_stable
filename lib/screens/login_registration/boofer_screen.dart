import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wish_app_ver_2/blocs/profile_bloc/bloc.dart';
import 'package:wish_app_ver_2/screens/bottom_tabs.dart';
import 'package:wish_app_ver_2/screens/login_registration/creating_profile_screen.dart';
import 'package:wish_app_ver_2/screens/login_registration/splash_screen.dart';
import 'package:wish_app_ver_2/src/models/models.dart';
import 'package:wish_app_ver_2/user_repository.dart';

class BufferScreen extends StatefulWidget {
  @override
  _BufferScreenState createState() => _BufferScreenState();
}

class _BufferScreenState extends State<BufferScreen> {
  ProfileBloc _profileBloc;
  UserRepository userRepository;

  @override
  void initState() {
    _profileBloc = BlocProvider.of<ProfileBloc>(context);
    super.initState();
  }

  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        return Scaffold(
          body: Row(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Container(
                  child: getViewAsPerState(state),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  getViewAsPerState(ProfileState state) {
    if (state is ProfileLoaded) {
      return BottomTabs();
    } else if (state is ProfileLoading) {
      return CircularProgressIndicator();
    } else if (state is ProfileEmpty) {
      return CreatingProfileScreen(
        onSave: (name, photo, dateOfBD) {
          BlocProvider.of<ProfileBloc>(context).add(
            AddProfile(Profile(
              name,
              photo,
              dateOfBD,
            )),
          );
        },
        isEditing: false,
      );
    }
  }
}
