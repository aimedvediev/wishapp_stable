//import 'package:flutter/material.dart';
//import 'package:flutter_svg/svg.dart';
//import 'package:wish_app_ver_2/screens/login_registration/registration_screen.dart';
//
//import '../../user_repository.dart';
//import 'login_screen.dart';
//
//class StartScreen extends StatelessWidget {
//  final UserRepository _userRepository;
//
//  StartScreen({Key key, @required UserRepository userRepository})
//      : assert(userRepository != null),
//        _userRepository = userRepository,
//        super(key: key);
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      body: Padding(
//        padding: const EdgeInsets.only(top: 170),
//        child: Center(
//          child: Column(
//            crossAxisAlignment: CrossAxisAlignment.center,
//            mainAxisAlignment: MainAxisAlignment.spaceBetween,
//            children: <Widget>[
//              Text(
//                'Hey there!',
//                style: TextStyle(
//                  fontSize: 22,
//                  fontFamily: 'Muli',
//                  fontWeight: FontWeight.bold,
//                  color: Color(0xFF362F35),
//                ),
//              ),
//              Padding(
//                padding: const EdgeInsets.only(
//                  top: 70,
//                ),
//                child: Container(
//                  child: SvgPicture.asset(
//                      "/Users/antonmedvediev/AndroidStudioProjects/wish_app_ver_2/lib/assets/icons/icon_gift_box.svg"),
//                ),
//              ),
//              Padding(
//                padding: const EdgeInsets.only(top: 100, bottom: 40),
//                child: Column(
//                  children: <Widget>[
//                    loginRegistrationButton(
//                      'LOGIN',
//                      () => Navigator.of(context).push(
//                        MaterialPageRoute(builder: (context) {
//                          return LoginScreen(userRepository: _userRepository);
//                        }),
//                      ),
//                    ),
//                    loginRegistrationButton(
//                      'REGISTRATION',
//                      () => Navigator.of(context).push(
//                        MaterialPageRoute(builder: (context) {
//                          return RegistrationScreen(userRepository: _userRepository);
//                        }),),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.only(top: 30),
//                      child: Container(
//                        padding: EdgeInsets.only(left: 25, right: 25),
//                        alignment: Alignment.topCenter,
//                        height: 40,
//                        width: 355,
//                        child: Text(
//                          'By continuing, you agree to our Terms od Service and Privacy Policy',
//                          textAlign: TextAlign.center,
//                          style: TextStyle(
//                            color: Color(0xFFCACBCC),
//                            fontSize: 13,
//                            fontFamily: 'Muli',
//                          ),
//                        ),
//                      ),
//                    ),
//                  ],
//                ),
//              ),
//            ],
//          ),
//        ),
//      ),
//    );
//  }
//}
//
//Widget loginRegistrationButton(String text, Function() onPress) {
//  return Padding(
//    padding: const EdgeInsets.only(top: 16),
//    child: ClipRRect(
//      borderRadius: new BorderRadius.circular(8.0),
//      child: Container(
//        height: 64,
//        width: 216,
//        color: text == 'LOGIN' ? Color(0xFF423932) : Color(0xFFDC6C54),
//        child: FlatButton(
//          onPressed: onPress,
//          child: Container(
//            child: Text(
//              text,
//              style: TextStyle(
//                color: Color(0xFFFFFFFF),
//                fontSize: 16,
//                fontFamily: 'OpenSans',
//                fontWeight: FontWeight.bold,
//                letterSpacing: 1.25,
//              ),
//            ),
//          ),
//        ),
//      ),
//    ),
//  );
//}
