import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:wish_app_ver_2/services/cloud_storage_result.dart';
import 'package:wish_app_ver_2/src/models/models.dart';
import 'package:wish_app_ver_2/widgets/buttons/change_apply_button.dart';
import 'package:wish_app_ver_2/widgets/custom_date_picker/date_picker.dart';
import 'package:wish_app_ver_2/widgets/photo_picker/image_selector.dart';
import 'package:wish_app_ver_2/widgets/text/text_field.dart';

typedef OnSaveCallback = Function(
  String name,
  String photo,
  String dateOfBD,
);

class CreatingProfileScreen extends StatefulWidget {
  static const routeName = '/profile_screen';

  final OnSaveCallback onSave;
  final Profile profile;
  final bool isEditing;

  const CreatingProfileScreen({
    Key key,
    @required this.onSave,
    this.profile,
    this.isEditing,
  }) : super(key: key);

  @override
  _CreatingProfileScreenState createState() => _CreatingProfileScreenState();
}

class _CreatingProfileScreenState extends State<CreatingProfileScreen> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _name;
  String _photo;

  String _dateOfBD = 'MM/DD/YYYY';

  bool validDate = true;
  bool validPhoto = true;
  bool uploadPhoto = false;
  File _selectedImage;

  bool get isEditing => widget.isEditing;

  Future<CloudStorageResult> uploadImage({
    @required File imageToUpload,
    @required String title,
  }) async {
    setState(() {
      uploadPhoto = true;
    });
    var imageFileName = title + DateTime.now().millisecondsSinceEpoch.toString();
//todo profile/phoneNr/member or present/imageFileName
    final StorageReference firebaseStorageRef = FirebaseStorage.instance.ref().child(imageFileName);

    if (widget.profile != null) {
      FirebaseStorage.instance
          .getReferenceFromUrl(widget.profile.photo)
          .then((value) => value.delete());
    }

    StorageUploadTask uploadTask = firebaseStorageRef.putFile(imageToUpload);
    StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;

    var downloadUrl = await storageTaskSnapshot.ref.getDownloadURL();

    if (uploadTask.isComplete) {
      var url = downloadUrl.toString();
      return CloudStorageResult(
        imageUrl: url,
        imageFileName: imageFileName,
      );
    }
    return null;
  }

  Future selectImage(File image) async {
    _selectedImage = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      image = _selectedImage;
      validPhoto = true;
    });
  }

  @override
  void initState() {
    isEditing ? _dateOfBD = widget.profile.dateOfBD : _dateOfBD = _dateOfBD;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFFFFFFF),
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xFFFFFFFF),
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      body: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            child: Text(
              isEditing ? '' : 'Welcome to Dono!',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0xFF362F35),
                fontSize: 22,
                fontWeight: FontWeight.bold,
                fontFamily: 'Muli',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 43, right: 42),
            child: Container(
              child: Text(
                isEditing
                    ? ''
                    : 'A few steps left. Filled the gaps with information needed to create your profile',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color(0xFFCACBCC),
                  fontSize: 14,
                  fontFamily: 'Opensans',
                  letterSpacing: 0.2,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 17, top: 45),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  TextTitleField(text: 'Member photo'),
                  Padding(
                    padding: const EdgeInsets.only(top: 12),
                    child: ImageSelector(
                      onSelection: () {
                        selectImage(_selectedImage);
                      },
                      image: _selectedImage,
                      valid: validPhoto,
                      isEditing: isEditing,
                      oldImage: isEditing ? widget.profile.photo : '',
                    ),
                  ),
                  TextTitleField(text: 'Name'),
                  Padding(
                    padding: const EdgeInsets.only(top: 12, right: 32),
                    child: Container(
                      height: 50,
                      padding: EdgeInsets.only(left: 12),
                      decoration: BoxDecoration(
                        borderRadius: new BorderRadius.circular(8.0),
                        border: Border.all(
                          width: 1,
                          color: Color(0xFFF4F5F5),
                        ),
                        color: Color(0xFFFFFFFF),
                      ),
                      child: TextFormField(
                        initialValue: isEditing ? widget.profile.name : '',
                        style: TextStyle(
                          color: Color(0xFF423932),
                          fontSize: 16,
                          letterSpacing: 0.25,
                          fontFamily: 'Muli',
                        ),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Type Name',
                        ),
                        onSaved: (value) => _name = value,
                        validator: (value) {
                          if (value.length < 2) {
                            return 'Name must have at least 3 symbols';
                          }
                          return null;
                        },
                      ),
                    ),
                  ),
                  TextTitleField(text: 'Date of Birth'),
                  CustomDatePicker(
                    text: _dateOfBD,
                    valid: validDate,
                    onSelection: () {
                      DatePicker.showDatePicker(context,
                          showTitleActions: true,
                          minTime: DateTime(1900, 1, 1),
                          maxTime: DateTime.now(), onConfirm: (date) {
                        var day = date.day.toString();
                        var month = date.month.toString();
                        var year = date.year.toString();
                        setState(() {
                          validDate = true;
                          _dateOfBD = '$day ${giveMonth(month)} $year';
                        });
                      }, locale: LocaleType.en);
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 64),
                    child: uploadPhoto
                        ? Center(
                            child: Container(
                              alignment: Alignment.center,
                              height: 64,
                              width: 133,
                              child: CircularProgressIndicator(),
                            ),
                          )
                        : ChangeApplyButton(
                            text: isEditing ? 'CHANGE' : 'CREATE',
                            onPressed: () async {
                              if (_dateOfBD == 'YYYY/MM/DD') {
                                setState(() {
                                  validDate = false;
                                });
                              }
                              if (_selectedImage == null) {
                                setState(() {
                                  validPhoto = false;
                                });
                              }
                              _formKey.currentState.save();
                              if (_formKey.currentState.validate() && validDate) {
                                if (_selectedImage != null) {
                                  CloudStorageResult storageResult;
                                  storageResult = await uploadImage(
                                      imageToUpload: _selectedImage, title: 'Profile');
                                  _photo = storageResult.imageUrl;
                                } else {
                                  _photo = widget.profile.photo;
                                }
                                await widget.onSave(_name, _photo, _dateOfBD);
                              }
                              if (isEditing) {
                                Navigator.of(context).pop();
                              }
                            },
                          ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
