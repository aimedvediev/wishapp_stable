import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wish_app_ver_2/blocs/login_bloc/bloc.dart';
import 'package:wish_app_ver_2/screens/login_registration/phone_form.dart';


import '../../user_repository.dart';

class PhoneScreen extends StatelessWidget {
  final UserRepository userRepository;

  PhoneScreen({Key key, @required this.userRepository})
      : assert(userRepository != null),
        super(key: key);

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFFFFFFF),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xFFFFFFFF),
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      body: BlocProvider<LoginBloc>(
          create: (context) => LoginBloc(userRepository: userRepository),
          child: PhoneForm(),
      ),
    );
  }
}

Widget loginRegistrationButton(String text, Function() onPress) {
  return Padding(
    padding: const EdgeInsets.only(top: 16),
    child: ClipRRect(
      borderRadius: new BorderRadius.circular(8.0),
      child: Container(
        height: 64,
        width: 216,
        color: text == 'LOGIN' ? Color(0xFF423932) : Color(0xFFDC6C54),
        child: FlatButton(
          onPressed: onPress,
          child: Container(
            child: Text(
              text,
              style: TextStyle(
                color: Color(0xFFFFFFFF),
                fontSize: 16,
                fontFamily: 'OpenSans',
                fontWeight: FontWeight.bold,
                letterSpacing: 1.25,
              ),
            ),
          ),
        ),
      ),
    ),
  );
}

Widget codeFormField() {
  return Padding(
    padding: const EdgeInsets.only(left: 16),
    child: ClipRRect(
      borderRadius: new BorderRadius.circular(8.0),
      child: Container(
        height: 50,
        width: 40,
        color: Color(0xFFEDEFF0),
        child: Container(
          alignment: Alignment.center,
          child: TextFormField(
            textAlign: TextAlign.center,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: '0',
            ),
          ),
        ),
      ),
    ),
  );
}
