import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pin_entry_text_field/pin_entry_text_field.dart';
import 'package:wish_app_ver_2/blocs/login_bloc/bloc.dart';
import 'package:wish_app_ver_2/blocs/authentication_block/bloc.dart';

class PhoneForm extends StatefulWidget {
  @override
  _PhoneFormState createState() => _PhoneFormState();
}

class _PhoneFormState extends State<PhoneForm> {
  LoginBloc _loginBloc;

  @override
  void initState() {
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      bloc: _loginBloc,
      listener: (context, loginState) {
        if (loginState is ExceptionState || loginState is OtpExceptionState) {
          String message;
          if (loginState is ExceptionState) {
            message = loginState.message;
          } else if (loginState is OtpExceptionState) {
            message = loginState.message;
          }
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {
          return Scaffold(
            resizeToAvoidBottomInset: false,
            backgroundColor: Color(0xFFFFFFFF),
            body: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    color: Color(0xFFFFFFFF),
                    child: Column(
                      children: <Widget>[
                        Header(),
                        Container(
                          child: getViewAsPerState(state),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  getViewAsPerState(LoginState state) {
    if (state is Unauthenticated) {
      return NumberInput();
    } else if (state is OtpSentState || state is OtpExceptionState) {
      return OtpInput();
    } else if (state is LoadingState) {
      return LoadingIndicator();
    } else if (state is LoginCompleteState) {
      BlocProvider.of<AuthenticationBloc>(context).add(LoggedIn(token: state.getUser().uid));
    } else {
      return NumberInput();
    }
  }
}

class Header extends StatelessWidget {
  const Header({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 48, left: 49, top: 144),
      child: Container(
        height: 50,
        alignment: Alignment.center,
        child: Text(
          'To easily share gifts and see gifts from the lists of others, we need your phone number with country code',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Color(0xFF655B53),
            fontFamily: 'Muli',
            fontSize: 13,
            letterSpacing: 0.4,
          ),
        ),
      ),
    );
  }
}

class LoadingIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Center(
        child: CircularProgressIndicator(),
      );
}

class NumberInput extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final _phoneTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.only(left: 50, top: 32, right: 18),
            child: Padding(
              padding: const EdgeInsets.only(top: 12, right: 32),
              child: Container(
                height: 50,
                padding: EdgeInsets.only(left: 12),
                decoration: BoxDecoration(
                  borderRadius: new BorderRadius.circular(8.0),
                  border: Border.all(
                    width: 1,
                    color: Color(0xFFF4F5F5),
                  ),
                  color: Color(0xFFFFFFFF),
                ),
                child: TextFormField(
                  controller: _phoneTextController,
                  keyboardType: TextInputType.phone,
                  style: TextStyle(
                    color: Color(0xFF423932),
                    fontSize: 16,
                    letterSpacing: 0.25,
                    fontFamily: 'Muli',
                  ),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Country code and phone number',
                  ),
                ),
              ),
            ),
          ),
        ),
        //todo button sent and back in widget
        Padding(
          padding: const EdgeInsets.only(top: 48),
          child: ClipRRect(
            borderRadius: new BorderRadius.circular(8.0),
            child: Container(
              height: 64,
              width: 216,
              child: RaisedButton(
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    BlocProvider.of<LoginBloc>(context)
                        .add(SendOtpEvent(phoNo: _phoneTextController.value.text));
                  }
                },
                color: Color(0xFF6DC5C0),
                child: Text(
                  "Sent",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  String validateMobile(String value) {
// Indian Mobile number are of 10 digit only

    return null;
  }
}

class OtpInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      child: Padding(
        padding: const EdgeInsets.only(top: 48, bottom: 16.0, left: 16.0, right: 16.0),
        child: Column(
          children: <Widget>[
            PinEntryTextField(
                fields: 6,
                onSubmit: (String pin) {
                  BlocProvider.of<LoginBloc>(context).add(VerifyOtpEvent(otp: pin));
                }),
            Padding(
              padding: const EdgeInsets.only(top: 48),
              child: ClipRRect(
                borderRadius: new BorderRadius.circular(8.0),
                child: Container(
                  height: 64,
                  width: 216,
                  child: RaisedButton(
                    onPressed: () {
                      BlocProvider.of<LoginBloc>(context).add(AppStartEvent());
                    },
                    color: Color(0xFF6DC5C0),
                    child: Text(
                      "Sent",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      constraints: BoxConstraints.tight(Size.fromHeight(250)),
    );
  }
}

class EditTextUtils {
  TextFormField getCustomEditTextArea({
    String labelValue = "",
    String hintValue = "",
    Function validator,
    IconData icon,
    bool validation,
    TextEditingController controller,
    TextInputType keyboardType = TextInputType.text,
    String validationErrorMsg,
  }) {
    return TextFormField(
      controller: controller,
      keyboardType: keyboardType,
      decoration: InputDecoration(
        prefixIcon: Icon(icon),
        prefixStyle: TextStyle(color: Colors.orange),
        fillColor: Colors.white.withOpacity(0.6),
        filled: true,
        isDense: true,
        labelStyle: TextStyle(color: Colors.orange),
        focusColor: Colors.orange,
        border: new OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            const Radius.circular(8.0),
          ),
          borderSide: new BorderSide(
            color: Colors.orange,
            width: 1.0,
          ),
        ),
        disabledBorder: new OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            const Radius.circular(8.0),
          ),
          borderSide: new BorderSide(
            color: Colors.orange,
            width: 1.0,
          ),
        ),
        focusedBorder: new OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            const Radius.circular(8.0),
          ),
          borderSide: new BorderSide(
            color: Colors.orange,
            width: 1.0,
          ),
        ),
        hintText: hintValue,
        labelText: labelValue,
      ),
      validator: validator,
    );
  }
}
