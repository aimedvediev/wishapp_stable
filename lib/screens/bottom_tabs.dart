import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wish_app_ver_2/blocs/my_present_bloc/dart.dart';
import 'package:wish_app_ver_2/screens/profile_screen.dart';

import 'home_screen.dart';
import 'my_members_screen.dart';
import 'my_presents_screen.dart';

class BottomTabs extends StatefulWidget {
  static const routeName = '/bottom_tabs';

  @override
  _BottomTabsState createState() => _BottomTabsState();
}

class _BottomTabsState extends State<BottomTabs> {
  List<Widget> _pages = [
    MyMembersScreen(),
    ProfileScreen(),
    MyPresentsScreen(),
  ];

  int _selectedPageIndex = 1;

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
//todo looks like KOSTYL

    return Scaffold(
      body: _pages[_selectedPageIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedPageIndex,
        onTap: _selectPage,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        iconSize: 32,
        items: [
          BottomNavigationBarItem(
            title: Text('Member'),
            icon: Icon(Icons.people),
          ),
          BottomNavigationBarItem(
            title: Text('Home'),
            icon: Icon(Icons.home),
          ),
          BottomNavigationBarItem(
            title: Text('Wish'),
            icon: Icon(Icons.list),
          ),
        ],
      ),
    );
  }
}
