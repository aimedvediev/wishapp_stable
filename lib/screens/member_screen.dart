import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:focused_menu/focused_menu.dart';
import 'package:focused_menu/modals.dart';

import 'package:wish_app_ver_2/blocs/member_bloc/bloc.dart';
import 'package:wish_app_ver_2/blocs/my_present_bloc/dart.dart';
import 'package:wish_app_ver_2/blocs/present_bloc/bloc.dart';
import 'package:wish_app_ver_2/members_repository.dart';
import 'package:wish_app_ver_2/screens/creating_editing_member_screen.dart';
import 'package:wish_app_ver_2/screens/myPresent_screen.dart';
import 'package:wish_app_ver_2/screens/present_screen.dart';
import 'package:wish_app_ver_2/widgets/custom_fab.dart';
import 'package:wish_app_ver_2/widgets/present_item.dart';

import 'package:wish_app_ver_2/widgets/text/title_grey_text.dart';
import 'package:wish_app_ver_2/widgets/date_card.dart';
import 'package:wish_app_ver_2/widgets/image_name_card.dart';
import 'package:wish_app_ver_2/widgets/buttons/small_button.dart';

typedef OnSaveCallback = Function(
  String phoneNr,
);

class MemberScreen extends StatefulWidget {
  static const routeName = '/member_screen';
  final OnSaveCallback onTap;

  MemberScreen({Key key, @required this.onTap}) : super(key: key);

  @override
  _MemberScreenState createState() => _MemberScreenState();
}

class _MemberScreenState extends State<MemberScreen> {
  var myList = true;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    final phoneNr = ModalRoute.of(context).settings.arguments as String;

    return BlocBuilder<MembersBloc, MembersState>(builder: (context, state) {
      final member = (state as MembersLoaded)
          .members
          .firstWhere((member) => member.id == phoneNr, orElse: () => null);
      return Scaffold(
        floatingActionButton: CustomFab(
          memberId: member.phoneNr,
          memberPhoto: member.photo,
          groups: member.group,
        ),
        backgroundColor: Color(0xffFAFAFA),
        body: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              backgroundColor: Color(0xFFFFFFFF),
              expandedHeight: 270,
              pinned: true,
              floating: true,
              iconTheme: IconThemeData(
                color: Colors.black,
              ),
              title: TitleGreyText(text: greyGroupText(member.group)),
              actions: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: SmallButton(
                    opacity: 1,
                    icon: Icons.edit,
                    onPress: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return CreatingEditingMemberScreen(
                          onSave: (name, phoneNr, groups, photo, dateOfBD) {
                            final presents =
                                BlocProvider.of<PresentsBloc>(context).state
                                    as PresentLoaded;
                            final present = presents.presents
                                .where((present) =>
                                    present.forWhomId == member.phoneNr)
                                .toList();
                            present.forEach((present) =>
                                BlocProvider.of<PresentsBloc>(context).add(
                                    UpdatePresent(present.copyWith(
                                        forWhomId: phoneNr,
                                        name: present.name,
                                        description: present.description,
                                        photo: present.photo,
                                        dateOfAdd: present.dateOfAdd,
                                        link: present.link,
                                        groups: groups,
                                        memberPhoto: photo,
                                        received: present.received))));
                            BlocProvider.of<MembersBloc>(context).add(
                                UpdateMember(member.copyWith(
                                    name: name,
                                    phoneNr: phoneNr,
                                    photo: photo,
                                    dateOfBD: dateOfBD,
                                    group: groups)));
                          },
                          isEditing: true,
                          member: member,
                        );
                      }));
                    },
                  ),
                )
              ],
              flexibleSpace: FlexibleSpaceBar(
                background: Container(
                  color: Color(0xffFAFAFA),
                  child: Container(
                    decoration: new BoxDecoration(
                      color: Color(0xffFFFFFF),
                      borderRadius: new BorderRadius.circular(16.0),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 21,
                        top: 117,
                      ),
                      child: Column(
                        children: <Widget>[
                          AvatarName(
                            image: member.photo,
                            name: member.name,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 24),
                            child: Container(
                              height: 90,
                              //todo can be some problems with shadow
                              child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  itemCount: 1,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    //todo normal data
                                    return DateCart(
                                      date: member.dateOfBD.substring(
                                          0, member.dateOfBD.length - 5),
                                      type: 'Birthday',
                                    );
                                  }),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            //todo tab
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 19,
                  top: 25,
                ),
                child: Row(
                  children: <Widget>[
                    FlatButton(
                      child: Text(
                        'My List',
                        style: TextStyle(
                          color: myList ? Color(0xFF3C332C) : Color(0xFFDBE0E6),
                          fontSize: 18,
                          fontWeight:
                              myList ? FontWeight.bold : FontWeight.normal,
                          fontFamily: 'Muli',
                          letterSpacing: 0.25,
                        ),
                      ),
                      onPressed: () {
                        setState(() {
                          myList = true;
                        });
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 50),
                      child: FlatButton(
                        child: Text(
                          '${member.name} List',
                          style: TextStyle(
                            color:
                                !myList ? Color(0xFF3C332C) : Color(0xFFDBE0E6),
                            fontSize: 18,
                            fontWeight:
                                !myList ? FontWeight.bold : FontWeight.normal,
                            fontFamily: 'Muli',
                            letterSpacing: 0.25,
                          ),
                        ),
                        onPressed: () {
                          setState(() {
                            myList = false;
                          });
                          widget.onTap(phoneNr);
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            myList
                ? BlocBuilder<PresentsBloc, PresentState>(
                    builder: (context, state) {
                      if (state is PresentLoading) {
                        return SliverToBoxAdapter(
                          child: CircularProgressIndicator(),
                        );
                      } else if (state is PresentLoaded) {
                        final presents = state.presents
                            .where((present) =>
                                present.forWhomId == member.phoneNr)
                            .toList();
                        return SliverList(
                          delegate: SliverChildBuilderDelegate(
                            (BuildContext context, int index) {
                              final present = presents[index];
                              return FocusedMenuHolder(
                                menuOffset: 20,
                                menuWidth:
                                    MediaQuery.of(context).size.width * 0.50,
                                blurBackgroundColor: Color(0xFFFAFAFA),
                                menuItems: <FocusedMenuItem>[
                                  FocusedMenuItem(
                                      title: Text(
                                        "Delete",
                                        style:
                                            TextStyle(color: Colors.redAccent),
                                      ),
                                      trailingIcon: Icon(
                                        Icons.delete,
                                        color: Colors.redAccent,
                                      ),
                                      onPressed: () {
                                        _showMyDialog(present);
                                      }),
                                  FocusedMenuItem(
                                      title: Text(
                                        "Recived",
                                        style: TextStyle(
                                            color: Colors.greenAccent),
                                      ),
                                      trailingIcon: Icon(
                                        Icons.add_shopping_cart,
                                        color: Colors.greenAccent,
                                      ),
                                      onPressed: () {
                                        _showMyDialog(present);
                                      }),
                                ],
                                child: PresentItem(
                                  present: present,
                                  onTap: () async {
                                    final removedMember =
                                        await Navigator.of(context).push(
                                      MaterialPageRoute(builder: (_) {
                                        return PresentScreen(
                                          presentId: present.id,
                                        );
                                      }),
                                    );
                                  },
                                ),
                              );
                            },
                            childCount: presents.length,
                          ),
                        );
                      } else {
                        return SliverToBoxAdapter(child: Container());
                      }
                    },
                  )
                : BlocBuilder<MyPresentsBloc, MyPresentState>(
                    builder: (context, state) {
                      if (state is MyPresentLoading) {
                        return SliverToBoxAdapter(
                          child: CircularProgressIndicator(),
                        );
                      } else if (state is MyPresentLoaded) {
                        final myPresents = state.myPresents;
                        return SliverList(
                          delegate: SliverChildBuilderDelegate(
                            (BuildContext context, int index) {
                              final myPresent = myPresents[index];
                              return FocusedMenuHolder(
                                blurBackgroundColor: Color(0xFFFAFAFA),
                                menuOffset: 20,
                                menuWidth:
                                    MediaQuery.of(context).size.width * 0.50,
                                menuItems: <FocusedMenuItem>[
                                  FocusedMenuItem(
                                      title: Text(
                                        "Reserve for present",
                                        style:
                                            TextStyle(color: Color(0xFF333333)),
                                      ),
                                      trailingIcon: Icon(
                                        Icons.add_shopping_cart,
                                        color: Color(0xFF333333),
                                      ),
                                      onPressed: () {}),
                                ],
                                child: PresentItem(
                                  myPresent: myPresent,
                                  onTap: () async {
                                    final removedMember =
                                        await Navigator.of(context).push(
                                      MaterialPageRoute(builder: (_) {
                                        return MyPresentScreen(
                                          presentId: myPresent.id,
                                          memberPhone: member.phoneNr,
                                        );
                                      }),
                                    );
                                  },
                                ),
                              );
                            },
                            childCount: myPresents.length,
                          ),
                        );
                      } else {
                        return SliverToBoxAdapter(child: Container());
                      }
                    },
                  )
          ],
        ),
      );
    });
  }

  Future<void> _showMyDialog(Present present) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Delete ${present.name}'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Would you like to delete present'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Yes'),
              onPressed: () {
                BlocProvider.of<PresentsBloc>(context)
                    .add(DeletedPresent(present));
                Navigator.pop(context, present);
              },
            ),
          ],
        );
      },
    );
  }

  String greyGroupText(GROUPS groups) {
    return groups == GROUPS.FAMILY
        ? 'Family Member'
        : groups == GROUPS.FRIENDS
            ? 'Friend Member'
            : groups == GROUPS.MYSELF ? 'My Profile' : 'Work Member';
  }
}

//todo
class ScreenArguments {
  final String id;
  final bool myList;

  ScreenArguments(this.id, this.myList);
}
