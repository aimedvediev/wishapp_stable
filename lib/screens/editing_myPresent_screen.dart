import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:wish_app_ver_2/my_present_repository.dart';
import 'package:wish_app_ver_2/present_repository.dart';
import 'package:wish_app_ver_2/services/cloud_storage_result.dart';

import 'package:wish_app_ver_2/widgets/buttons/change_apply_button.dart';
import 'package:wish_app_ver_2/widgets/photo_picker/image_selector.dart';
import 'package:wish_app_ver_2/widgets/text/text_field.dart';
import 'package:wish_app_ver_2/widgets/text/title_grey_text.dart';

typedef OnSaveCallback = Function(
    String forWhomId,
    String name,
    String description,
    String photo,
    String dateOfAdd,
    String link,
    GROUPS groups,
    bool received,
    );

class EditingMyPresentScreen extends StatefulWidget {
  static const routeName = '/creating_editing_present_screen';

  final OnSaveCallback onSave;
  final MyPresent myPresent;
  final bool isEditing;

  EditingMyPresentScreen({
    Key key,
    @required this.onSave,
    this.myPresent,
    this.isEditing,
  }) : super(key: key);

  @override
  _EditingMyPresentScreenState createState() =>
      _EditingMyPresentScreenState();
}

class _EditingMyPresentScreenState
    extends State<EditingMyPresentScreen> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _forWhomId;

  String _name;
  String _description;
  String _photo;

  String _dateOfAdd =
  DateFormat('d MMMM y').format(DateTime.now()).toString();

  GROUPS _groups = GROUPS.ALL;
  String _link = '';
  bool _received = false;

  var validPhoto = true;

  List<GROUPS> groupsList = [
    GROUPS.FAMILY,
    GROUPS.FRIENDS,
    GROUPS.COLLEAGUES,
  ];

  GROUPS selectedGroup = GROUPS.FAMILY;

  File _selectedImage;

  bool get isEditing => widget.isEditing;

  Future<CloudStorageResult> uploadImage({
    @required File imageToUpload,
    @required String title,
  }) async {
    var imageFileName =
        title + DateTime.now().millisecondsSinceEpoch.toString();
//todo profile/phoneNr/member or present/imageFileName
    final StorageReference firebaseStorageRef =
    FirebaseStorage.instance.ref().child(imageFileName);

    if (widget.myPresent != null) {
      FirebaseStorage.instance
          .getReferenceFromUrl(widget.myPresent.photo)
          .then((valued) => valued.delete());
    }
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(imageToUpload);
    StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;

    var downloadUrl = await storageTaskSnapshot.ref.getDownloadURL();

    if (uploadTask.isComplete) {
      var url = downloadUrl.toString();
      return CloudStorageResult(
        imageUrl: url,
        imageFileName: imageFileName,
      );
    }
    return null;
  }

  Future selectImage(File image) async {
    _selectedImage = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      image = _selectedImage;
      validPhoto = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    final memberId = ModalRoute.of(context).settings.arguments as String;
    _forWhomId = memberId;
    return Scaffold(
      backgroundColor: Color(0xFFFFFFFF),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xFFFFFFFF),
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        title: TitleGreyText(
            text: isEditing ? 'Edit My Present' : 'Add New Present'),
      ),
      body: ClipRRect(
        borderRadius: new BorderRadius.circular(16.0),
        child: Container(
          alignment: Alignment.topLeft,
          padding: const EdgeInsets.only(left: 20, top: 27),
          width: width,
          color: Color(0xFFFAFAFA),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 12),
                  child: Container(
                    height: 50,
                    child: TextFormField(
                      initialValue: isEditing ? widget.myPresent.name : '',
                      keyboardType: TextInputType.text,
                      style: TextStyle(
                        fontSize: 22,
                        fontFamily: 'Muli',
                        fontWeight: FontWeight.bold,
                      ),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'New Present',
                      ),
                      onSaved: (value) => _name = value,
                      validator: (value) {
                        if (value.length < 2) {
                          return 'Name must have at least 3 symbols';
                        }
                        return null;
                      },
                    ),
                  ),
                ),

                TextTitleField(text: 'Add photo'),
                //TODO picker for photo and edit photo badge
                Padding(
                  padding: const EdgeInsets.only(top: 12),
                  child: ImageSelector(
                    onSelection: () {
                      selectImage(_selectedImage);
                    },
                    image: _selectedImage,
                    valid: validPhoto,
                    type: 'present',
                    isEditing: isEditing,
                    oldImage: isEditing ? widget.myPresent.photo : '',
                  ),
                ),
                TextTitleField(text: 'Description'),
                Padding(
                  padding: const EdgeInsets.only(top: 12, right: 32),
                  child: Container(
                    height: 50,
                    padding: EdgeInsets.only(left: 12),
                    decoration: BoxDecoration(
                      borderRadius: new BorderRadius.circular(8.0),
                      border: Border.all(
                        width: 1,
                        color: Color(0xFFF4F5F5),
                      ),
                      color: Color(0xFFFFFFFF),
                    ),
                    child: TextFormField(
                      initialValue: isEditing ? widget.myPresent.description : '',
                      style: TextStyle(
                        color: Color(0xFF423932),
                        fontSize: 16,
                        letterSpacing: 0.25,
                        fontFamily: 'Muli',
                      ),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Type description',
                        hintStyle: TextStyle(
                          color: Color(0xFFCACBCC),
                          fontSize: 16,
                          letterSpacing: 0.25,
                          fontFamily: 'Muli',
                        ),
                      ),
                      validator: (value) {
                        if (value.length < 5) {
                          return 'Name must have at least 5 symbols';
                        }
                        return null;
                      },
                      onSaved: (value) => _description = value,
                    ),
                  ),
                ),
//                TextTitleField(text: 'Link to present'),
//                CustomTextFormField(
//                  hint: 'Paste the link',
//                ),
                ChangeApplyButton(
                  text: isEditing ? 'CHANGE' : 'CREATE',
                  onPressed: () async {
                    if (_selectedImage == null) {
                      setState(() {
                        validPhoto = false;
                      });
                    }
                    if (_formKey.currentState.validate()) {
                      if (_selectedImage != null) {
                        CloudStorageResult storageResult;
                        storageResult = await uploadImage(
                            imageToUpload: _selectedImage,
                            title: _forWhomId == null ? 'profile' : _forWhomId);
                        _photo = storageResult.imageUrl;
                      } else {
                        _photo = widget.myPresent.photo;
                      }
                      _formKey.currentState.save();
                      await widget.onSave(
                        _forWhomId,
                        _name,
                        _description,
                        _photo,
                        _dateOfAdd,
                        _link,
                        _groups,
                        _received,
                      );
                      Navigator.pop(context);
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
