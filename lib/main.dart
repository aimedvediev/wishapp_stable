import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wish_app_ver_2/blocs/authentication_block/authentication_bloc.dart';
import 'package:wish_app_ver_2/blocs/member_bloc/bloc.dart';
import 'package:wish_app_ver_2/blocs/my_present_bloc/dart.dart';
import 'package:wish_app_ver_2/blocs/present_bloc/bloc.dart';

import 'package:wish_app_ver_2/blocs/simple_bloc_delegate.dart';
import 'package:wish_app_ver_2/members_repository.dart';
import 'package:wish_app_ver_2/my_present_repository.dart';
import 'package:wish_app_ver_2/profile_repository.dart';

import 'package:wish_app_ver_2/screens/bottom_tabs.dart';
import 'package:wish_app_ver_2/screens/creating_editing_member_screen.dart';
import 'package:wish_app_ver_2/screens/creating_editing_present_screen.dart';
import 'package:wish_app_ver_2/screens/login_registration/phone_screen.dart';
import 'package:wish_app_ver_2/screens/login_registration/splash_screen.dart';
import 'package:wish_app_ver_2/screens/member_screen.dart';
import 'package:wish_app_ver_2/screens/present_screen.dart';
import 'package:wish_app_ver_2/src/firebase_present_repository.dart';
import 'package:wish_app_ver_2/user_repository.dart';

import 'blocs/authentication_block/bloc.dart';
import 'blocs/profile_bloc/bloc.dart';

import 'screens/login_registration/boofer_screen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  final UserRepository userRepository = UserRepository();
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              AuthenticationBloc(userRepository)..add(AppStarted()),
        ),
        BlocProvider<MembersBloc>(
          create: (context) => MembersBloc(
            membersRepository: FirebaseMemberRepository(),
          )..add(LoadMembers()),
        ),
        BlocProvider<PresentsBloc>(
          create: (context) => PresentsBloc(
            presentRepository: FirebasePresentRepository(),
          )..add(LoadPresents()),
        ),
        BlocProvider<MyPresentsBloc>(
          create: (context) => MyPresentsBloc(
            myPresentRepository: FirebaseMyPresentRepository(),
          ),
        ),
        BlocProvider<ProfileBloc>(
          create: (context) => ProfileBloc(
            profileRepository: FirebaseProfileRepository(),
          ),
        ),
      ],
      child: MyApp(userRepository: userRepository),
    ),
  );
}

class MyApp extends StatelessWidget {
  final UserRepository _userRepository;

  MyApp({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: Colors.black,
        buttonColor: Colors.black,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context, state) {
          if (state is Uninitialized) {
            return SplashScreen();
          } else if (state is Unauthenticated) {
            return PhoneScreen(
              userRepository: _userRepository,
            );
          } else if (state is Authenticated) {
            return BlocProvider<ProfileBloc>(
                create: (context) =>
                    ProfileBloc(profileRepository: FirebaseProfileRepository())
                      ..add(LoadProfile()),
                child: BufferScreen());
          } else {
            return SplashScreen();
          }
        },
      ),
      routes: {
        PresentScreen.routeName: (ctx) => PresentScreen(),
        MemberScreen.routeName: (ctx) => MemberScreen(
              onTap: (memberPhone) {
                print(memberPhone);
                BlocProvider.of<MyPresentsBloc>(context)
                    .add(LoadMyPresents(memberPhone));
              },
            ),
        CreatingEditingMemberScreen.routeName: (ctx) =>
            CreatingEditingMemberScreen(
              onSave: (name, phoneNr, groups, photo, dateOfBD) {
                BlocProvider.of<MembersBloc>(context).add(
                  AddMember(
                      Member(name, phoneNr, photo, dateOfBD, group: groups)),
                );
              },
              isEditing: false,
            ),
        CreatingEditingPresentScreen.routeName: (ctx) =>
            CreatingEditingPresentScreen(
              onSave: (forWhomId, name, description, photo, dateOfAdd, link,
                  groups, received, memberPhoto) {
                if (forWhomId == '0') {
                  BlocProvider.of<MyPresentsBloc>(context).add(
                    AddMyPresent(MyPresent(forWhomId, name, description, photo,
                        dateOfAdd, link, groups, received)),
                  );
                } else {
                  BlocProvider.of<PresentsBloc>(context).add(
                    AddPresent(
                      Present(forWhomId, name, description, photo, dateOfAdd,
                          link, groups, received, memberPhoto),
                    ),
                  );
                }
              },
              isEditing: false,
            ),
        BottomTabs.routeName: (ctx) => BottomTabs()
      },
    );
  }
}
